"use strict";

const isDev = require('electron-is-dev');

const electron = require('electron');
const {remote} = electron;
const importLazy = require('import-lazy')(require);
const fs = importLazy('fs-extra');
const currentWindow = remote.getCurrentWindow();

var appmod = angular.module('app', ['ngMaterial', 'ngStorage', 'ui.bootstrap.contextMenu',
									'metaData', 'updateCheck', 'reader', 'downloader']);

appmod.config(['$mdThemingProvider', '$provide', '$compileProvider', function ($mdThemingProvider, $provide, $compileProvider)
{
	$mdThemingProvider.generateThemesOnDemand(true);
	$provide.value('themeProvider', $mdThemingProvider);

	$compileProvider.debugInfoEnabled(isDev);
}]);

appmod.controller('appCtrl', ['$rootScope', '$scope', '$localStorage', 'hlpShared', 'testFac',
function($rootScope, $scope, $localStorage, hlpShared, testFac)
{
	if(isDev)
	{
		console.log($scope);
		console.log(testFac);
	}

	var defaultconfig =
	{
		config:
		{
			search:"",
			dlreppath:"%MediaTitle%/v%Volume%/c%Chapter%/%FileName%.%DataType%",
			format:"raw",
			dldir:remote.app.getPath("downloads"),
			mmodeloptions:{},
			fetchRemoteScript:false,
			enableBackgroundThrottling:false,
			colorTheme:
			{
				primaryPalette:'light-blue',
				accentPalette:'red',
				warnPalette:'yellow',
				backgroundPalette:'grey',
				darkTheme:true
			}
		},
		reader:
		{
			config:
			{
				show:false,
				zoomFac:1,
				dragscroll:false,
				enableFileHistory:true,
				enableEndlessScroll:false,
				enableSmoothScroll:true,
				hideSideMenu:false,
				videoAutoPlay:false,
				videoLoop:false,
				videoVolume:0.5,
				videoPlaybackRate:1,
				basedir:remote.app.getPath("downloads"),
				styleContainer:{"overflow": "hidden", "background-color":"rgb(0,0,0)"},
				styleImage:{"max-width": "100%"},
				keyConfig:
				[
					{type:"ArrowLeft",  fct:"changeFile",       x:-1  },
					{type:"ArrowRight", fct:"changeFile",       x: 1  },
					{type:"ArrowDown",  fct:"scrollVertical",   x: 15 },
					{type:"ArrowUp",    fct:"scrollVertical",   x:-15 },
					{type:"-",          fct:"changeZoom",       x:-0.1},
					{type:"+",          fct:"changeZoom",       x: 0.1},
					{type:"F5",         fct:"reloadApp"               },
					{type:"F9",         fct:"toggleReader"            },
					{type:"F10",        fct:"toggleSideMenu"          },
					{type:"F11",        fct:"toggleFullscreen"        },
					{type:"F12",        fct:"toggleDevTools"          },
					{type:"PageUp",     fct:"changeFile",       x:-25 },
					{type:"PageDown",   fct:"changeFile",       x: 25 },
					{type:"Numpad1",    fct:"scrollHorizontal", x:-15 },
					{type:"Numpad2",    fct:"scrollHorizontal", x: 15 },
					{type:"Delete",     fct:"deleteCurrentFile"       }
				],
				mouseConfig:
				[
					{type:"LeftClick",  fct:"changeFile",       x: 1  },
					{type:"WheelClick", fct:"toggleSideMenu",         },
					{type:"RightClick", fct:"changeFile",       x:-1  },
					{type:"Backward",   fct:"changeFile",       x:-1  },
					{type:"Forward",    fct:"changeFile",       x: 1  },
					{type:"WheelUp",    fct:"scrollVertical",   x:-15 },
					{type:"WheelDown",  fct:"scrollVertical",   x: 15 },
				]
			},
			readerFileHistory:{},
			readerLastOpenDir:""
		}
	};

	$rootScope.$storage = $localStorage.$default(defaultconfig);
	hlpShared.addMissingProps(defaultconfig,$scope.$storage);
	hlpShared.updateColorTheme(true);
	hlpShared.setBackgroundThrottling();
}]);
