var request = importLazy('request');

appmod.factory('mmShared',['$http','$q','hlpShared',

function ($http,$q,hlpShared)
{
	
	var factory = {};
	
	factory.zeropad = hlpShared.zeropad;
	
	factory.normalizeChaNumb = function(chanumb)
	{
		if(!isNaN(parseFloat(chanumb)))
		{
			var normchanumb = factory.zeropad(parseInt(chanumb),4);
			chanumb=chanumb.toString();
			if(chanumb.indexOf(".")>=0)
				return normchanumb + chanumb.substring(chanumb.indexOf("."),chanumb.length);
			else
				return normchanumb;
		}
		else
			return chanumb;
	}
	factory.normalizeVolNumb = function(volnumb)
	{
		if(!isNaN(parseFloat(volnumb)))
			return factory.zeropad(volnumb,4);
		else
			return volnumb;
	}


	factory.nodeRequestPromise = function(requestCfg,requestlist)
	{
		return new Promise(function(resolve, reject)
		{
			try
			{
				var req = request(Object.assign({timeout:60000, gzip: true}, requestCfg),
				function (error, response, body)
				{
					if(response)
						response.data = body;
					resolve(response); 
				});
				if(requestlist)
					requestlist.push(req);
			}
			catch(e)
			{
				reject(e);
			}
		});
	}
	
	factory.cancelmPromises = function(start,passthrough,mmodel)
	{
		// debugger;
		if(start == 0)
			mmodel.cancelMlist = false;

		for(k=start;k<passthrough.cancelerlist.length;k++)
		{
			if(passthrough.cancelerlist[k] && passthrough.cancelerlist[k].resolve)
				passthrough.cancelerlist[k].resolve();
			if(passthrough.requestlist[k] && passthrough.requestlist[k].abort)
				passthrough.requestlist[k].abort();
		}
		
	}
	factory.mProcessResponse = function(body,passthrough,i,mmodel)
	{		
		var dom = new DOMParser().parseFromString(body, "text/html");
		
		if(mmodel&&mmodel.cancelMlist)
			factory.cancelmPromises(0,passthrough,mmodel);
		if(i==0&&passthrough.options.options.getmaxpage)
			passthrough.options.options.gesiter = parseInt(passthrough.options.options.getmaxpage(dom));
		
		var elems = passthrough.options.selectFct(dom) || [];
		var mlistPart = [];
		for(elem of elems)
		{
			var pushelems = elem;
			if(passthrough.options.elementFct)
				pushelems = passthrough.options.elementFct(elem,dom,i);
			if(!pushelems)
				continue;
			if(!Array.isArray(pushelems))
				pushelems=[pushelems];
			
			for(pushelem of pushelems)
				if(pushelem&&pushelem.link)
				{	
					pushelem.name = pushelem.name.trim();
					mlistPart.push(pushelem);
				}
				else 
					if(passthrough.options.debug)
						console.log("debug", {"method":"commonMextract","element":pushelem,"index":i,"warning":"received mlist element without link"});
		}
		
		if(passthrough.options.debug)
			console.log("debug", {"method":"commonMextract","index":i,"mlistPart":mlistPart});
		
		if(passthrough.options.options.cancelOnEmpty && elems.length == 0)
			factory.cancelmPromises(i,passthrough,mmodel);

		return mlistPart;
	}
	factory.mPartPromise = function(passthrough,i,mmodel)
	{
		return new Promise(async function(resolve, reject)
		{
			(async function(i,mmodel)
			{
				try
				{
					var requestCfg = JSON.parse(JSON.stringify(passthrough.options.requestCfg));
					if(typeof passthrough.options.requestCfg.url == "function")
						requestCfg.url = passthrough.options.requestCfg.url(i);
					
					var response;
					if(passthrough.options.options.nodeRequest)
					{
						passthrough.cancelerlist[i] = {resolve:resolve};
						response = await factory.nodeRequestPromise(requestCfg, passthrough.requestlist);
					}
					else
						response = await $http( Object.assign({method:'GET', timeout: passthrough.cancelerlist[i].promise, headers: {'Cache-Control' : 'no-cache'}}, requestCfg));
					
					if(passthrough.options.debug)
						console.log("debug", {"method":"commonMextract","index":i,"serverResponse":response});
					passthrough.mlist = passthrough.mlist.concat(factory.mProcessResponse(response.data,passthrough,i,mmodel));

					if(!passthrough.options.options.latency)
						resolve();
					else
						setTimeout(resolve, passthrough.options.options.latency);
				}
				catch(e)
				{
					if(e.status != -1)
						console.error(e);
					//reject hangs $q.all whatever
					resolve();
				}
			})(i,mmodel);
		});
	}
	factory.commonMextract = async function(options)
	{
		var passthrough = {mlist:[],httplist:[],cancelerlist:[],requestlist:[],options:options};
		
		passthrough.options.options = options.options || {};
		passthrough.options.options.gesiter = options.options.gesiter || 1;
		
		if(options.debug)
			console.log("debug", {"method":"commonMextract","input":options});
		
		if(options.options.nodeRequest)
			passthrough.cancelerlist = Array(options.options.gesiter);
		else
			passthrough.cancelerlist = Array(options.options.gesiter).fill($q.defer())
		
		for(var i=0;i<passthrough.options.options.gesiter;i++)
		{				
			if(i>0&&!options.options.latency)
				passthrough.httplist.push(factory.mPartPromise(passthrough,i,hlpShared.currentmmodel));
			else
				await factory.mPartPromise(passthrough,i,hlpShared.currentmmodel);
		}
		
		await $q.all(passthrough.httplist);

		if(passthrough.options.debug)
			console.log("debug", {"method":"commonMextract","mlist":passthrough.mlist});
		return passthrough.mlist;
	}

	factory.cProcessResponse = async function(body,options)
	{
		var chapters=[];
		var dom = new DOMParser().parseFromString(body, "text/html");
		var elems = await options.selectFct(dom);
		
		for(elem of elems)
		{	
			var pushelems = elem;
			if(options.elementFct)
				pushelems = await options.elementFct(elem,dom);
			if(!pushelems)
				continue;
			if(!Array.isArray(pushelems))
				pushelems = [pushelems];
			
			for(pushelem of pushelems)
			{
				if(!pushelem)
					continue;
				
				var prevcha = null;
				if(pushelem.volnumb == null || pushelem.chanumb == null)
					for(var k=0;k<chapters.length;k++)
						if(chapters[k].version == pushelem.version)
							prevcha = chapters[k];
				if(pushelem.volnumb == null)
				{
					if(chapters.length==0 || !prevcha)
						pushelem.volnumb = 1;
					else
						pushelem.volnumb = parseInt(prevcha.volnumb);
				}
				pushelem.volnumb = factory.normalizeVolNumb(pushelem.volnumb);
				
				if(pushelem.chanumb == null)
				{
					if(chapters.length==0 || !prevcha)
						pushelem.chanumb = 1;
					else
						pushelem.chanumb = parseInt(prevcha.chanumb) + 1;
				}
				
				pushelem.chanumb = factory.normalizeChaNumb(pushelem.chanumb);
				chapters.push(pushelem);
			}
		}
		
		if(options.debug)
			console.log("debug", {"method":"commonCextract","chapters":chapters});
		
		return chapters;
		
	}
	factory.commonCextract = async function(options)
	{
		if(options.debug)
			console.log("debug", {"method":"commonCextract","input":options});

		try
		{
			var response;
			if(options.options && options.options.nodeRequest)
				response = await factory.nodeRequestPromise(options.requestCfg);
			else
				response = await $http(Object.assign({method:'GET', headers: {'Cache-Control' : 'no-cache'}}, options.requestCfg));

			if(options.debug)
				console.log("debug", {"method":"commonCextract","serverResponse":response});
			var chapters = await factory.cProcessResponse(response.data,options);

			if(chapters.length==0)
				throw new Error("chapters array empty");
			else
				return chapters;
		}
		catch(e)
		{
			throw e;
		}
	}
	
	return factory;
}]
);