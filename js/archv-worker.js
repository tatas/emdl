var importLazy = require('import-lazy')(require);
var zipdir = importLazy('zip-dir');
var getImageSize = importLazy('image-size');
var PDFDocument = importLazy('pdfkit');
var fs = importLazy('fs');

var packCbz = function(ev)
{
	try
	{
		var dir = (ev.data.dirpath).replace(/\\/g,"/");
		zipdir(dir, { saveTo: dir+'.cbz' },
		function (err, buffer)
		{
			postMessage("archv finished");
		});	
	}
	catch(e)
	{
		postMessage({ "debug" : e.stack });
	}
}
var packPdf = function(ev)
{
	try
	{
		var sortFilesHelper = function(a, b)
		{
			var aarr=a.toLowerCase().split("/");
			var barr=b.toLowerCase().split("/");
		
			if(aarr.length<barr.length)
				return -1;
			if(aarr.length>barr.length)
				return 1;
			
			for(var i=0;i<aarr.length;i++)
			{
				if(aarr[i]<barr[i])
					return -1;
				if(aarr[i]>barr[i])
					return 1;
			}
			return 0;
		};
		var sortFiles = function(a, b)
		{
			var sortVal = sortFilesHelper(a.path, b.path);
			if(sortVal)
				return sortVal;
			return sortFilesHelper(a.zipPath, b.zipPath);
		};

		var fileList = ev.data.fileList.sort(sortFiles);
		var imageSize = getImageSize(fileList[0].path);
		var doc = new PDFDocument({size:[imageSize.width,imageSize.height]});
		doc.image(fileList[0].path, 0, 0, imageSize);

		for(var i=1; i<fileList.length; i++)
		{
			try
			{
				imageSize = getImageSize(fileList[i].path);
				doc.addPage({size:[imageSize.width,imageSize.height]});
				doc.image(fileList[i].path, 0, 0, imageSize);
			}
			catch(e){}
		}

		doc.end();
		var writeStream = fs.createWriteStream(ev.data.dirpath+".pdf");
		doc.pipe(writeStream);
		writeStream.on('finish', function ()
		{
			postMessage("generating pdf finished");
		});
	}
	catch(e)
	{
		postMessage({ "debug" : e.stack });
	}
}

onmessage = function (ev)
{
	if(ev.data.format=="cbz")
		packCbz(ev);
	
	if(ev.data.format=="pdf")
		packPdf(ev);
};