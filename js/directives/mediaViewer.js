var setEndlessIndex;

angular.module("reader").factory('mediaViewerFactory', function()
{
    var factory = {}; 
    return factory;
});

angular.module("reader").directive("mediaViewer",
function($timeout, mediaViewerFactory)
{
    return {
		restrict : 'E',
		scope: {},
        templateUrl : 'js/directives/mediaViewer.html',
		link: function(scope, element, attrs, controllers)
		{
			if(isDev)
				console.log(scope, mediaViewerFactory);

			scope.$watch(() => mediaViewerFactory.config, () => scope.config = mediaViewerFactory.config);
			scope.$watch(() => mediaViewerFactory.openm,  () => scope.openm  = mediaViewerFactory.openm);
			scope.abs = Math.abs;
			$timeout( () =>
			{
				initVideo();
				mediaViewerFactory.setZoom(scope.config.zoomFac);
				currentWindow.on('enter-full-screen', enterFullScreen);
				currentWindow.on('leave-full-screen', leaveFullScreen);
			}, 0);

			var domElems = {};
			var domIds = ["viewerContainer", "fileIndex", "filesLength", "videoContainer", "videoDisplay", "imageDisplay"];
			for(var domId of domIds)
				domElems[domId] = document.getElementById(domId);
			domElems.imagePrefetch = [{img:new Image(), offset:-1, decode:false}, {img:new Image(), offset:1, decode:true}];
			scope.setScrollContainer = () => domElems.scrollContainer = document.getElementById("scrollContainer");
			mediaViewerFactory.setFilesLength = () => domElems.filesLength.innerText = scope.openm.files.length;

			let initVideo = function()
			{
				domElems.videoContainer.ondragstart = (event) => 
				{
					event.preventDefault();
					videoDragging = true;
					var canvas = document.createElement('canvas');
					canvas.height = domElems.videoDisplay.videoHeight*(150/domElems.videoDisplay.videoWidth);
					canvas.width = 150;
					canvas.getContext('2d').drawImage(domElems.videoDisplay, 0, 0, domElems.videoDisplay.videoWidth, domElems.videoDisplay.videoHeight, 0, 0, canvas.width, canvas.height);
					ipcRenderer.send('ondragstart', scope.openm.files[scope.openm.fileindex].path, canvas.toDataURL());
				}
	
				if(scope.config.videoVolume||scope.config.videoVolume===0)
					domElems.videoDisplay.volume = scope.config.videoVolume;
				domElems.videoDisplay.autoplay = scope.config.videoAutoPlay;
				
				scope.$watch("config.videoAutoPlay",() => domElems.videoDisplay.autoplay = scope.config.videoAutoPlay);
				scope.$watch("config.videoLoop",() => domElems.videoDisplay.loop = scope.config.videoLoop);
				domElems.videoDisplay.addEventListener('volumechange', () => domElems.videoDisplay.muted ?	scope.config.videoVolume = 0 : scope.config.videoVolume = domElems.videoDisplay.volume);
				domElems.videoDisplay.addEventListener('ratechange', () => {
					scope.config.videoPlaybackRate = domElems.videoDisplay.playbackRate;
					domElems.videoDisplay.defaultPlaybackRate = scope.config.videoPlaybackRate;
				});
				domElems.videoDisplay.defaultPlaybackRate = scope.config.videoPlaybackRate;
				domElems.videoDisplay.playbackRate = scope.config.videoPlaybackRate;

				
				scope.videoPlayer = videojs('videoDisplay', 
				{
					"autoplay": scope.config.videoAutoPlay,
					"loop": scope.config.videoLoop,
					"controls": true,
					"fluid": true,
					"playbackRates": [0.25, 0.5, 0.75, 1, 1.5, 2],
					"controlBar": { "fullscreenToggle": false }
				});
			}


			mediaViewerFactory.dispOverlay = function()
			{ 
				scope.overmousetime = performance.now();
				scope.$apply( () => scope.config.showOverlay = true);
				setTimeout(function()
				{
					if(performance.now()-scope.overmousetime>=1000)
						scope.$apply( () => scope.config.showOverlay = false);
				},1000);
			}
			mediaViewerFactory.setZoom = function(zoomfac)
			{
				var elems = document.getElementsByClassName("mediaDisplay");
				for(var i = 0; i<elems.length;i++)
					elems[i].style.zoom = parseFloat(zoomfac);
				domElems.videoDisplay && (domElems.videoDisplay.style.zoom = parseFloat(zoomfac));
			}
			scope.setZoom = mediaViewerFactory.setZoom;
			mediaViewerFactory.changeZoom = function(i)
			{
				scope.config.zoomFac += i;
				scope.config.zoomFac = Math.max(scope.config.zoomFac, 0.1);
				mediaViewerFactory.setZoom(scope.config.zoomFac);
			}
			var mouseDownTimeout = null;
			var mouseDownTimeFactor = 5;
			scope.uiMouseDown = function(i, type)
			{
				mouseDownTimeout = window.setTimeout(() =>
				{
					mouseDownTimeFactor = Math.max(1, mouseDownTimeFactor-1);
					if(type == "fileindex")
						mediaViewerFactory.changeFileIndex(scope.openm.fileindex+i);
					if(type == "zoom")
					{
						mediaViewerFactory.changeZoom(i);
						scope.$digest();
					}
					scope.uiMouseDown(i, type);
				}, 90*(mouseDownTimeFactor==5 ? 0 : mouseDownTimeFactor));
			}
			scope.uiMouseUp = function()
			{
				mouseDownTimeFactor = 5;
				window.clearTimeout(mouseDownTimeout);
			}

			var fileChangeCounter = 0;
			var fileChangeTimeout = null;
			var getSafeFileIndex = (index) => Math.min(Math.max(index,0),scope.openm.files.length-1);
			var setMediaDisplaySrc = function()
			{
				if(scope.config.enableEndlessScroll && !scope.$root.$$phase)
				{	
					scope.$digest();
					return;
				}

				if(decodeURI(domElems.imageDisplay.src).replace("file:///","") != scope.openm.files[scope.openm.fileindex].src)
				{
					domElems.viewerContainer.scrollTop=0; 
					domElems.viewerContainer.scrollLeft=0;
					skrollTop.stop();
				}
				else
					return;
					
				if(scope.openm.files[scope.openm.fileindex] && scope.openm.files[scope.openm.fileindex].src)
					domElems.imageDisplay.src = scope.openm.files[scope.openm.fileindex].src;	

				if(scope.openm.files[scope.openm.fileindex].mediaType=='video')
				{	
					scope.videoPlayer && scope.videoPlayer.src({src:scope.openm.files[scope.openm.fileindex].src, type: scope.openm.files[scope.openm.fileindex].mimeType});
					domElems.imageDisplay.style.display = "none";
					domElems.videoContainer.style.display = "block";
				}
				else
				{
					scope.videoPlayer && scope.videoPlayer.pause();
					domElems.imageDisplay.style.display = "block";
					domElems.videoContainer.style.display = "none";
				}

				for(var prefetch of domElems.imagePrefetch)
				{
					if(scope.openm.files[getSafeFileIndex(scope.openm.fileindex - prefetch.offset)].src)
					{
						prefetch.img.src = scope.openm.files[getSafeFileIndex(scope.openm.fileindex - prefetch.offset)].src;
						// prefetch.decode && prefetch.img.decode().catch(() => {});
					}
				}
			}
			var loadZipImageData = async function()
			{
				var prefetchFactor = scope.config.enableEndlessScroll ? 9 : 1;
				var prefetchFrom = getSafeFileIndex(scope.openm.fileindex-prefetchFactor);
				var prefetchTo = getSafeFileIndex(scope.openm.fileindex+prefetchFactor);
				for(var i=0; i<scope.openm.files.length; i++)
				{
					if(scope.openm.files[i].zipFile && (i<prefetchFrom || i>prefetchTo))
					{	
						URL.revokeObjectURL(scope.openm.files[i].src);
						scope.openm.files[i].src = null;
						scope.openm.files[i].loading = false;
					}
				}
				for(var i=prefetchFrom; i<=prefetchTo; i++)
					(async function(index)
					{
						if(!scope.openm.files[index].src && !scope.openm.files[index].loading)
						{	
							scope.openm.files[index].loading = true;
							var bufs = [];
							var mediaStream = await scope.openm.files[index].zipFile.openReadStream();
							mediaStream.on('data', function(d){ bufs.push(d); });
							mediaStream.on('end', function()
							{
								scope.openm.files[index].src = URL.createObjectURL(new Blob([ Buffer.concat(bufs) ]));
								if(scope.config.enableEndlessScroll)
								{
									for(var i=prefetchFrom; i<=scope.openm.fileindex; i++)
										if(!scope.openm.files[i].src)
											return;
									setMediaDisplaySrc();
								}
								if(!scope.config.enableEndlessScroll&&index==scope.openm.fileindex)
									setMediaDisplaySrc();
							});
						}
					})(i);
			}
			mediaViewerFactory.changeFileIndex = function(newind)
			{
				if(scope.openm.files.length == 0)
				{
					domElems.imageDisplay.style.display = "none";
					domElems.videoContainer.style.display = "none";
					return;
				}

				scope.openm.fileindex = getSafeFileIndex(newind);
				domElems.fileIndex.innerText = scope.openm.fileindex+1;					
				mediaViewerFactory.onFileChange && mediaViewerFactory.onFileChange();

				if(scope.config.enableEndlessScroll)
				{	
					scope.resetIndex=true;
					if(scope.openm.fileindex<=9 || scope.openm.fileindex>=scope.openm.files.length-9)
						setEndlessIndex();
				}

				if(scope.openm.files[scope.openm.fileindex].zipFile)
					loadZipImageData();

				if(scope.openm.files[scope.openm.fileindex].src)
				{
					if(fileChangeTimeout)
						window.clearTimeout(fileChangeTimeout);
					fileChangeTimeout = window.setTimeout(() => 
					{
						fileChangeCounter = 0;
						setMediaDisplaySrc();
					},100);					
					fileChangeCounter++;

					if(fileChangeCounter<=5)
					{	
						setMediaDisplaySrc();
						window.clearTimeout(fileChangeTimeout);
					}
					if(fileChangeCounter==6)
						setMediaDisplaySrc();
				}
			}
			

			scope.$watch("config.enableEndlessScroll", () => 
			{
				if(scope.config.enableEndlessScroll !== undefined && scope.openm)
					mediaViewerFactory.changeFileIndex(scope.openm.fileindex)
			});
			scope.isFullScreen = currentWindow.isFullScreen();
			var enterFullScreen = () => { scope.isFullScreen = true;  scope.$digest(); };
			var leaveFullScreen = () => { scope.isFullScreen = false; scope.$digest(); };
			window.addEventListener('beforeunload', ()=>
			{
				currentWindow.removeListener('enter-full-screen', enterFullScreen);				
				currentWindow.removeListener('leave-full-screen', leaveFullScreen);
			});
			domElems.viewerContainer.parentNode.addEventListener("mousemove", function(ev)
			{
				if(scope.config.showOverlay != (ev.clientY<100 || ev.clientX<100))
					scope.$apply( () => scope.config.showOverlay = !scope.config.showOverlay);
			});
			domElems.viewerContainer.parentNode.addEventListener("mouseleave", function(ev)
			{
				//Sometimes while switching files via ui the event triggers (also on the document listener) at the click location. Probably an electron bug, but only the lord knows why.
				if(scope.config.showOverlay && (ev.clientX<0 || ev.clientY<0 || ev.clientX>=domElems.viewerContainer.clientWidth || ev.clientY>=domElems.viewerContainer.clientHeight))
					scope.$apply( () => scope.config.showOverlay = false);
			});

			//scrolling spaghetti
			setEndlessIndex = function()
			{
				if(!scope.resetIndex)
					return;
					
				for(let childElement of domElems.scrollContainer.children)
				{
					if(!childElement.complete)
						return;
						
					if(Number(childElement.getAttribute("fileIndex")) == scope.openm.fileindex)
					{
						scope.resetIndex=false;
						childElement.scrollIntoView();
						break;
					}
				}
			}
			let setEndlessScrollFileIndex = function()
			{
				let found = false;
				for(let childElement of domElems.scrollContainer.children)
				{	
					if(childElement.y>0)
					{
						scope.openm.fileindex=Number(childElement.getAttribute("fileIndex"))-1;
						found = true;
						break;
					}
				}
				if(!found)
					scope.openm.fileindex = Number(domElems.scrollContainer.children[domElems.scrollContainer.children.length-1].getAttribute("fileIndex"));
				
				loadZipImageData();
				mediaViewerFactory.onFileChange && mediaViewerFactory.onFileChange();
				scope.$digest();
			}

			let isSmoothScrolling = false;
			var stopScolling = () =>
			{
				skrollTop.stop();
				if(!scope.config.enableSmoothScroll && scope.config.enableEndlessScroll)
					setEndlessScrollFileIndex();
			}
			let handleSmoothScroll = function(xOffset,yOffset)
			{
				isSmoothScrolling = true;
				skrollTop.scrollTo({
					element: domElems.viewerContainer,
					easing: (t, b, c, d) => b+c*t/8,
					scrollDirection: yOffset != 0 ? "scrollTop" : "scrollLeft",
					to:              yOffset != 0 ? domElems.viewerContainer.scrollTop+yOffset : domElems.viewerContainer.scrollLeft+xOffset,
					duration: 999999,
					callback: () =>{
						isSmoothScrolling = false;
						if(scope.config.enableEndlessScroll)
							setEndlessScrollFileIndex();
					}
				});
			}
			mediaViewerFactory.handleScroll = function(xOffset,yOffset)
			{
				if(!isSmoothScrolling && scope.config.enableSmoothScroll)
					handleSmoothScroll(xOffset,yOffset);
				else if(!scope.config.enableSmoothScroll)
				{
					domElems.viewerContainer.scrollLeft+=xOffset;
					domElems.viewerContainer.scrollTop+=yOffset;
				}
			}


			//key handling
			let keyMap = {};
			scope.execKeyFct = (key) =>
			{
				mediaViewerFactory.keyBindFcts[key.fct](key.x);
				if(!mediaViewerFactory.keyBindFcts[key.fct].noDigest)
					scope.$digest();
			}
			mediaViewerFactory.getModifierString = () => Object.keys(keyMap).join("+");
			domElems.viewerContainer.addEventListener("mousewheel", function(ev)
			{
				scope.execKeyFct(scope.config.mouseConfig[ev.deltaY<0 ? 5 : 6]);
				clearTimeout(scope.scrollStopTimeout);
				scope.scrollStopTimeout = setTimeout(stopScolling, 100);
			});
			domElems.viewerContainer.addEventListener("mouseup", function(ev)
			{
				if(ev.target.className.split("-")[0]!="vjs")
					scope.execKeyFct(scope.config.mouseConfig[ev.button]);
			});
			document.addEventListener("keydown", function(ev)
			{
				keyMap[ev.code] = true;
				if(ev.target.tagName == "INPUT" || ev.target.tagName == "TEXTAREA")
					return;

				var modifierString = mediaViewerFactory.getModifierString(ev);
				for(let key of scope.config.keyConfig)
				{
					if(modifierString == key.type)
					{
						ev.preventDefault();
						scope.execKeyFct(key);
						return;
					}
				}
			}, true);
			document.addEventListener("keyup", function(ev)
			{
				var modifierString = mediaViewerFactory.getModifierString(ev);
				delete keyMap[ev.code];
				for(let key of scope.config.keyConfig)
				{
					if(modifierString == key.type && (key.fct=="scrollVertical" || key.fct=="scrollHorizontal"))
					{
						stopScolling();
						return;
					}
				}
			}, true);

		}
    };
});
	