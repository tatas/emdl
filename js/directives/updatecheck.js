angular.module("updateCheck", ['ngMaterial']).directive("updateCheckDialog",
['$rootScope', '$http', '$mdDialog',	
function($rootScope, $http, $mdDialog)
{
    return {
        restrict : 'E',
        templateUrl : 'js/directives/updatecheck.html',
		link: function(scope)
		{
			if(isDev)
				console.log(scope);

			scope.notifyversion = $rootScope.$storage.config.notifyversion;
			scope.changeNotifyVersion = function()
			{
				if(scope.notifyversion != scope.updversion)
					scope.notifyversion = scope.updversion;
				else
					scope.notifyversion = null;
			}
			$http(
			{ 
				method: 'GET',
				url: "https://copynippyjoin.gitlab.io/emdl-website/version",
				headers: 
				{
					'Cache-Control' : 'no-cache'
				}
			})
			.then(
				function(response)
				{
					scope.curversion = remote.app.getVersion();
					scope.updversion = response.data;
					
					scope.showUpdateDialog = scope.curversion != scope.updversion && scope.notifyversion != scope.updversion;
					if(scope.showUpdateDialog)
					{
						$mdDialog.show(
						{
							contentElement: '#updateDialog',
							parent: angular.element(document.body),
							clickOutsideToClose: true,
							onComplete:function()
							{
								var updatewv = document.getElementsByTagName("webview")[0];
								updatewv.src = "https://copynippyjoin.gitlab.io/emdl-website/";
								scope.updgoback = function() {updatewv.goBack(); };
								updatewv.getWebContents().on('did-navigate', function(event, oldUrl, newUrl)
								{
									scope.$apply(function () {scope.updshowback = updatewv.canGoBack();});
								});
							}
						});	
					}
				},
				function(response){console.warn({"desc":"failed to connect to the update server","receiveddata":response});}
			);
		}
    };
}]);
	