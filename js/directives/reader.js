const yauzl = importLazy( 'yauzl-promise');
const trash = importLazy("trash");
const ipcRenderer = importLazy('electron').ipcRenderer

const mime = importLazy('mime-types');
const walk = importLazy('walk');

angular.module("reader", ['ngMaterial', 'ng-drag-scroll']).directive("readerUi",
function($rootScope, $mdDialog, hlpShared, mediaViewerFactory)
{
    return {
		restrict : 'E',
        scope: {},
        templateUrl : 'js/directives/reader.html',
		link: function(scope, element, attrs, controllers)
		{
			// TODO: unspaghetti endless scroll; make viewer overlay extensible; some oop stuff; file sorting > pdf pack;
			if(isDev)
				console.log(scope);
		
			//init
			scope.hlpShared = hlpShared;
			scope.storage = $rootScope.$storage.reader;
			scope.config = scope.storage.config;
			scope.mediaViewerFactory = mediaViewerFactory;
			mediaViewerFactory.config = scope.config;
			scope.mdDialog = $mdDialog;

			let readerBaseDir = document.getElementById("readerBaseDir");
			scope.rootdirs = [];
			scope.openm = {};
			scope.walkerid = 0;
			scope.parseInt = parseInt;

			scope.menuFolders = 
			[
				['Open', function ($itemScope, $event, modelValue, text, $li) {
					scope.config.basedir = (modelValue || $itemScope.dir).path.replace(/\\/g, "/");
				}],
				null,
				['Open in explorer', function ($itemScope, $event, modelValue, text, $li) {
					electron.shell.openExternal((modelValue || $itemScope.dir).path.replace(/\\/g, "/"));
				}],
				null,
				['Read', function ($itemScope, $event, modelValue, text, $li) {
					scope.openNewFolder(modelValue || $itemScope.dir);
				}],
				null,
				['Rename', async function ($itemScope, $event, modelValue, text, $li) {
					await renameFileModal((modelValue || $itemScope.dir).path);
					loadBaseDir();
				}],
				null,
				['Delete', async function ($itemScope, $event, modelValue, text, $li) {
					scope.newFolderName = (modelValue || $itemScope.dir).path;
					await scope.hlpShared.showDialog($event,'#folderDelete');
					scope.rootdirs.splice($itemScope.$index, 1);
					setTimeout(()=>{trash([scope.newFolderName]).then(() => {}).catch((e) => { console.error('error deleting folder',e); });});
				}],
				null,
				['Metadata', function ($itemScope, $event, modelValue, text, $li) {
					hlpShared.showMetaInfo((modelValue || $itemScope.dir).name,$event);
				}]
			];
					
			scope.setTitle = function()
			{
				var pathTitle = "emdl | ";
				if(scope.openm.files[scope.openm.fileindex])
				{
					pathTitle += scope.openm.files[scope.openm.fileindex].path;
					if(scope.openm.files[scope.openm.fileindex].zipPath)
						pathTitle += scope.openm.files[scope.openm.fileindex].zipPath;
					pathTitle += " | " + (scope.openm.fileindex+1) + "/" + scope.openm.files.length;
				}
				else
					pathTitle += scope.openm.dir.path;
				currentWindow.setTitle(pathTitle);
			}
			mediaViewerFactory.onFileChange = () => 
			{
				scope.setTitle();
				if(!scope.openm.loading && scope.config.enableFileHistory)
					scope.storage.readerFileHistory[scope.openm.dir.path]=scope.openm.fileindex;
			}
			
			//listeners	
			document.addEventListener('dragover', function(ev)
			{
				ev.preventDefault();
				ev.dataTransfer.dropEffect = 'link';
			});
			document.addEventListener('drop', function(ev)
			{
				ev.preventDefault();
				if(ev.dataTransfer.files[0] && ev.dataTransfer.effectAllowed != "copyLink")
				{
					scope.openNewFolder({path:ev.dataTransfer.files[0].path});
					scope.$apply(() => scope.config.show = true);
				}
			});
			scope.$watch("config.basedir", () => scope.config.basedir && loadBaseDir());
			scope.$watch("config.show",    () => scope.config.show    && loadBaseDir());


			//ui help functions - settings
			scope.deleteFileHistElem = (key) => delete scope.storage.readerFileHistory[key];
			scope.importFileHistory = function()
			{
				remote.dialog.showOpenDialog((fileNames) =>
				{
					scope.storage.readerFileHistory = JSON.parse(fs.readFileSync(fileNames[0], 'utf-8'));
					scope.$digest();
				});
			};
			scope.exportFileHistory = function()
			{
				var dlElem = document.createElement('a');
				dlElem.href = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(scope.storage.readerFileHistory,null,4));
				dlElem.download = "fileHistory.json";
				dlElem.click();
			};
			scope.openOptionsDialog = function($event)
			{
				scope.showOptionsDialog = true;
				scope.hlpShared.showDialog($event,'#rdConfig', null, () => scope.showOptionsDialog = false);
			}
			scope.changeStyle = (elem,prop) => function(){scope.config[prop] = JSON.parse(elem.value)};
			scope.setKeyString = (event,key)=>
			{
				event.preventDefault(); 
				key.type = mediaViewerFactory.getModifierString();
			}
			let renameFileModal = async function(oldPath)
			{
				scope.newFolderName = oldPath;
				await scope.hlpShared.showDialog(false,'#folderRename');
				await fs.rename(oldPath, scope.newFolderName);
				return scope.newFolderName;
			}
			
			//sidemenu
			scope.baseDirGoUp = function()
			{
				var newdir = scope.config.basedir.replace(/\\/g, "/");
				if(newdir.split("/").length>1)
					newdir = newdir.substring(0,newdir.lastIndexOf("/"));
				scope.config.basedir = newdir;
			}
			scope.baseDirOpenDialog = function()
			{
				remote.dialog.showOpenDialog({properties: ['openDirectory']}, (fileNames) =>
				{
					scope.config.basedir = fileNames ? fileNames[0].replace(/\\/g, "/") : scope.config.basedir;
					scope.$digest();
				});
			}
			var loadBaseDir = function()
			{
				if(!scope.config.show)
					currentWindow.setTitle("emdl")
				else
				{
					hlpShared.valMdInput(readerBaseDir,
						() => scope.rootdirs = scope.hlpShared.getFilesNonRec(scope.config.basedir));

					if(!scope.openm.dir)
					{
						var dir = scope.config.enableFileHistory ? scope.storage.readerLastOpenDir : null;
						dir && scope.openNewFolder(dir);
					}
					else
						scope.setTitle();
				}
			}

			//load folder
			var pushOpenmFile = (fileData) =>
			{
				scope.openm.files.push(fileData);
				if(scope.openm.files.length==1)
					mediaViewerFactory.changeFileIndex(0);
				mediaViewerFactory.setFilesLength();
			}
			var getMediaType = (mime) =>
		    {    
				mime = mime ? mime.split('/')[0] : "";
				return mime == "audio" ? "video" : mime;
			}
			var isZip = (mime) => (mime=="application/x-cbr"||mime=="application/zip");
			var loadZip = async function(path,walkerId)
			{
				var zipFile = await yauzl.open(path);
				await zipFile.walkEntries( entry => {
					if(entry.compressedSize == 0 || scope.walkerid!=walkerId)
						return;
					let mimeType = mime.lookup(entry.fileName);
					pushOpenmFile({path:path, zipPath:"/"+entry.fileName, zipFile:entry, mimeType:mimeType, mediaType:getMediaType(mimeType)});
				});		
			}		
			var loadFolder = function()
			{
				var walker = walk.walk(scope.openm.dir.path, { followLinks: false });
				scope.walkerid++;
				walker.id = scope.walkerid;

				walker.on('file', async function(root, stat, next)
				{
					var filePath = root.replace(/\\/g, "/") + '/' + stat.name;			
					var mimeType = mime.lookup(filePath);
					if(isZip(mimeType))
						await loadZip(filePath,walker.id);
					else
						pushOpenmFile({path:filePath, src:filePath, mimeType:mimeType, mediaType:getMediaType(mimeType)});

					next();
				});	
				walker.on("names", function (root, nodeNamesArray)
				{
					nodeNamesArray.sort(function (a, b)
					{
						if (a > b) return -1;
						if (a < b) return 1;
						return 0;
					});
				});
				walker.on('end', filesLoadFinished);		
			}
			var filesLoadFinished = function()
			{
				scope.openm.loading = false;
				scope.resetIndex = true;
				scope.openm.files.sort(scope.hlpShared.sortFiles);
				if(scope.config.enableFileHistory)
					scope.storage.readerLastOpenDir = scope.openm.dir;

				if(scope.storage.readerFileHistory && scope.storage.readerFileHistory[scope.openm.dir.path] && scope.config.enableFileHistory)
					mediaViewerFactory.changeFileIndex(scope.storage.readerFileHistory[scope.openm.dir.path]);						
				else
					mediaViewerFactory.changeFileIndex(0);

				scope.$digest();
			}
			scope.openNewFolder = function(dir)
			{
				dir.path = dir.path.replace(/\\/g, "/");
				scope.storage.readerFileHistory = scope.storage.readerFileHistory || {};
				if(scope.config.enableFileHistory && (scope.openm.dir && scope.openm.dir.path && !scope.openm.loading))
					scope.storage.readerFileHistory[scope.openm.dir.path] = scope.openm.fileindex;	

				scope.openm = {dir:dir, files:[], loading:true};
				mediaViewerFactory.openm = scope.openm;
				scope.setTitle();
				loadFolder();
			}

			// key functions
			mediaViewerFactory.keyBindFcts = 
			{
				changeFile: (x) => mediaViewerFactory.changeFileIndex(scope.openm.fileindex+parseFloat(x)),
				scrollVertical: (x) => mediaViewerFactory.handleScroll(0,parseFloat(x)),
				changeZoom: (x) => 					
				{
					mediaViewerFactory.changeZoom(parseFloat(x));
					mediaViewerFactory.dispOverlay()
				},
				toggleFullscreen: () => currentWindow.setFullScreen(!currentWindow.isFullScreen()),
				toggleSideMenu: () => scope.$apply(() => scope.config.hideSideMenu = !scope.config.hideSideMenu),
				toggleReader: () => scope.$apply(() => scope.config.show = !scope.config.show),
				scrollHorizontal:  (x) => mediaViewerFactory.handleScroll(parseFloat(x),0),
				deleteCurrentFile: () => 
				{
					var path = scope.openm.files.splice(scope.openm.fileindex,1)[0].src;
					mediaViewerFactory.changeFileIndex(scope.openm.fileindex);
					mediaViewerFactory.setFilesLength();
					setTimeout(()=>{trash([path]).then(() => {}).catch((e) => { console.error('error deleting file',e); });});
				},
				renameCurrentFile: async () =>
				{
					var newFileName = await renameFileModal(scope.openm.files[scope.openm.fileindex].src);
					scope.openm.files[scope.openm.fileindex].src = newFileName;
					scope.openm.files[scope.openm.fileindex].path = newFileName;
					mediaViewerFactory.changeFileIndex(scope.openm.fileindex);
				},
				autoChangeFile: (x) =>
				{
					if(!scope.autoChangeFileInterval) 
					{	 	
						mediaViewerFactory.changeFileIndex(scope.openm.fileindex+1); 
						scope.autoChangeFileInterval = setInterval(()=>mediaViewerFactory.changeFileIndex(scope.openm.fileindex+1),parseFloat(x));
					}
					else
					{
						clearInterval(scope.autoChangeFileInterval);
						scope.autoChangeFileInterval = null;
					}
				},
				reloadApp: () => location.reload(),
				toggleDevTools: () => currentWindow.toggleDevTools(),
				customFunction: (x) => eval(x)
			}
			scope.keyBindFctsNames = [];
			for(let fctName in mediaViewerFactory.keyBindFcts)
			{
				scope.keyBindFctsNames.push(fctName);
				if(["changeFile", "scrollVertical", "scrollHorizontal", "toggleReader", "toggleSideMenu", "toggleFullscreen",
				    "reloadApp", "toggleDevTools", "customFunction"].includes(fctName))
					mediaViewerFactory.keyBindFcts[fctName].noDigest = true;
			}
		}
    };
});
	