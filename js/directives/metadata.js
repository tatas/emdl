angular.module("metaData", ['ngMaterial']).directive("metaDataDialog",
['$http', '$mdDialog', 'hlpShared',	
function($http, $mdDialog, hlpShared)
{
    return {
		restrict : 'E',
        templateUrl : 'js/directives/metadata.html',
		scope:
		{
		  reqname: '=',
		  reqevent: '='
		},
		link: function(scope, element, attrs, controllers)
		{
			if(isDev)
				console.log(scope);
			
			scope.getMetaData = async function(name)
			{   
				var baserequest = "site%3Amangaupdates.com%2Fseries.html+" + name.replace(/[^a-zA-Z0-9]/g,' ');
				scope.metaData = {};
				var metaData = scope.metaData;
				var muURL;
				var metaDataRequests = [
					{	
						options: { 
							method: 'POST',
							url:  "https://www.mangaupdates.com/series.html",
							headers: {'Content-Type':'application/x-www-form-urlencoded'},
							data: "act=series&session=00000000000000000000000000000000&stype=title&search=%22"+encodeURI(name.trim())+"%22&x=10&y=10"
						},
						callback: function(response)
						{
							var elem = new DOMParser().parseFromString(response.data, "text/html").getElementsByClassName("series_rows_table")[0].getElementsByTagName("a")[4];
							if(elem && elem.href.includes("mangaupdates.com/series.html?id="))
								return elem.href;
							else
								throw "mu no result";
						}
					},
					{	
						options: { 
							method: 'GET',
							url: "https://duckduckgo.com/html/?q=" + baserequest
						},
						callback: function(response)
						{
							var elem = new DOMParser().parseFromString(response.data, "text/html").getElementsByClassName("result__a");
							if(elem&&elem.length>0)
								return elem[0].href;
							else
								throw "duckduckgo no result";
						}
					},
					{	
						options: { 
							method: 'GET',
							url: "https://www.startpage.com/do/search?q="+baserequest
						},
						callback: function(response)
						{
							var elem = new DOMParser().parseFromString(response.data, "text/html").getElementById("title_1");
							if(elem)
								return elem.href;
							else
								throw "startpage no result";
						}
					}
				];

				for(metaDataRequest of metaDataRequests)
					try
					{
						var response = await $http(metaDataRequest.options);
						muURL = metaDataRequest.callback(response);
						if(!muURL)
							throw "got empty mu URL";
						break;
					}
					catch(e){}
				
				try
				{
					if(!muURL)
						throw "got empty mu URL";
					var response = await $http({method: 'GET', url: muURL});
					
					try
					{
						var html = new DOMParser().parseFromString(response.data, "text/html");
						metaData.link = response.config.url;
						metaData.name = html.getElementsByClassName("releasestitle tabletitle")[0].innerText;
						metaData.description = html.getElementsByClassName("sContainer")[0].getElementsByClassName("sContent")[0].innerText;
						metaData.type = html.getElementsByClassName("sContainer")[0].getElementsByClassName("sContent")[1].innerText.trim();
						metaData.relatedseries = html.getElementsByClassName("sContainer")[0].getElementsByClassName("sContent")[2].innerText.split("\n").filter(Boolean);
						metaData.asocnames  = html.getElementsByClassName("sContainer")[0].getElementsByClassName("sContent")[3].innerText.split("\n").filter(Boolean);
						
						var transarr = html.getElementsByClassName("sContainer")[0].getElementsByClassName("sContent")[5].innerText.split("\n").filter(Boolean);
						metaData.latesttrans  = transarr.splice(0,transarr.length-1);
						metaData.latestrels  = html.getElementsByClassName("sContainer")[0].getElementsByClassName("sContent")[6].innerText.split("\n").filter(Boolean);
						metaData.anstartend  = html.getElementsByClassName("sContainer")[0].getElementsByClassName("sContent")[8].innerText.split("\n").filter(Boolean);
						
						metaData.img  = html.getElementsByClassName("sContainer")[1].getElementsByTagName("img")[0].src;
						var genrearr = html.getElementsByClassName("sContainer")[1].getElementsByClassName("sContent")[1].innerText.replace(/\ /g,"").split(/\s/g);
						metaData.genre  = genrearr.splice(0,genrearr.length-3);
						var catarr = html.getElementsByClassName("sContainer")[1].getElementsByClassName("sContent")[2].innerText.split("\n").filter(Boolean);
						metaData.categories  = catarr.splice(1,catarr.length-1);
						metaData.authors  = html.getElementsByClassName("sContainer")[1].getElementsByClassName("sContent")[5].innerText.split("\n").filter(Boolean);
						metaData.artists  = html.getElementsByClassName("sContainer")[1].getElementsByClassName("sContent")[6].innerText.split("\n").filter(Boolean);
						metaData.year  = html.getElementsByClassName("sContainer")[1].getElementsByClassName("sContent")[7].innerText;
					}
					catch(e)
					{
						metaData.error = true;
						console.error(e);
					}
				}
				catch(e)
				{
					metaData.error = true;
					console.error(e);
				}
				scope.$digest();
			}
			
			scope.hlpShared = hlpShared;
			hlpShared.showMetaInfo = async function(reqname,reqevent)
			{
				scope.getMetaData(reqname);
				$mdDialog.show(
				{
					contentElement: '#metaInfo',
					parent: angular.element(document.body),
					targetEvent: reqevent,
					clickOutsideToClose: true,
					onRemoving:function(){scope.metaData = undefined;}
				});
			};
			
			scope.$watch('reqname',function()
			{
				if(scope.reqname)
					scope.showMetaInfo(scope.reqname,scope.reqevent);
			});
		}
    };
}]);
	