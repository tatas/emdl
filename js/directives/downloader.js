const Worker = importLazy("tiny-worker");

angular.module("downloader", ['ngMaterial']).directive("downloaderUi",
['$rootScope', '$http', '$q', 'mmShared', 'hlpShared', 'testFac',	
function($rootScope, $http, $q, mmShared, hlpShared, testFac)
{
    return {
		restrict : 'E',
        scope: {},
        templateUrl : 'js/directives/downloader.html',
		link: function($scope)
		{
			// TODO: unspaghetti shared functions;

			//init
			if(isDev)
				console.log($scope);
				
			var throttledDigest = hlpShared.throttle(function(){$scope.$digest();},200);
			$scope.colorPalettes = hlpShared.colorPalettes;
			$scope.updateColorTheme = hlpShared.updateColorTheme;
			$scope.mmShared = mmShared;
			$scope.hlpShared = hlpShared;
			$scope.currentm = {};
			$scope.currentmlist = {};
			$scope.mmodels = [];
			$scope.downloads = [];
			$scope.jsbase = __dirname.replace("app.asar","app.asar.unpacked")+"/js/";

			$scope.mlists = [];
			fs.readFile(remote.app.getPath("userData")+"/emdl-mlists.json", 'utf8',function(err,data)
			{
				try
				{
					if(!err)
						$scope.mlists = JSON.parse(data);
					else
						console.log(err);
				}
				catch(e)
				{console.log(e);}
				
				$scope.loadMmodels();
				testFac.mmodels = $scope.mmodels;
				$scope.$digest();
			});

			// load the mmodels
			$scope.loadMmodels = async function()
			{
				$scope.mmodels = [];
				var loadBasePath = $scope.jsbase+"mmodels/";
				
				if($rootScope.$storage.config.fetchRemoteScript)
				{
					loadBasePath = $scope.jsbase+"remoteMmodels/";
					var remoteScripts = await $http.get("https://gitlab.com/api/v4/projects/3342389/repository/tree?path=js%2Fmmodels&ref=master&recursive=true");
					remoteScripts = remoteScripts.data.map(resp => {return resp.path.split("js/mmodels/")[1];}).filter(str => {return str.split(".")[1];});
					var promises = [];
		
					await fs.remove(loadBasePath);
					var error = false;
					for(var script of remoteScripts)
						(function(script){
							promises.push(
							$http.get("https://gitlab.com/CopyNippyJoin/emdl/raw/master/js/mmodels/"+script).then(function(response)
							{
								if(!fs.existsSync(loadBasePath))
									fs.mkdirSync(loadBasePath);
								fs.writeFileSync(loadBasePath+script.split("/").splice(-1)[0],response.data);
							}).catch( e => error = true));
						})(script);	
					await $q.all(promises);
					if(error)
						loadBasePath = $scope.jsbase+"mmodels/";
				}
				
				var files = $scope.hlpShared.getFiles(loadBasePath);
				for(var i=0; i<files.length;i++)
					if((files[i].indexOf("-worker") == -1)&&(files[i].indexOf(".js") > -1))
					{		
		
						$scope.mmodels.push(require(files[i]));
						$scope.mmodels[$scope.mmodels.length-1].workerpath = (files[i]).replace(".js","-worker.js").replace(/\\/g,"/");
			
						if(isDev)
							fs.readFile($scope.mmodels[$scope.mmodels.length-1].workerpath, 'utf8',function(err,data)
							{
								var esprima = importLazy('esprima');
								try
								{
									var errors = esprima.parseScript(data, { tolerant: true, loc: true }).errors;
									if(errors.length>0)
										console.error($scope.mmodels[$scope.mmodels.length-1].workerpath,errors);
								}
								catch(e)
								{console.error($scope.mmodels[$scope.mmodels.length-1].workerpath,e);}
							});
					}			
			
				$scope.mmodels = $scope.mmodels.sort($scope.hlpShared.sortByAttr('name'));
				$scope.currentmmodel = $scope.mmodels[0];				
				var currentmmodelname = $rootScope.$storage.config.currentmmodelname;
		
				for(var i=0; i<$scope.mmodels.length;i++)
				{
					if(currentmmodelname == $scope.mmodels[i].name)
						$scope.currentmmodel = $scope.mmodels[i];
					if(!currentmmodelname && "mangaseeonline" == $scope.mmodels[i].name)
						$scope.currentmmodel = $scope.mmodels[i];
		
					if($rootScope.$storage.config.mmodeloptions[$scope.mmodels[i].name])
					{
						hlpShared.addMissingProps($scope.mmodels[i].options,$rootScope.$storage.config.mmodeloptions[$scope.mmodels[i].name]);
						$scope.mmodels[i].options = $rootScope.$storage.config.mmodeloptions[$scope.mmodels[i].name];
					}
					else
						$rootScope.$storage.config.mmodeloptions[$scope.mmodels[i].name] = $scope.mmodels[i].options;
				}
				
				if(!$rootScope.$$phase)
					$scope.$digest();
			}
				
		
			//ui help functions
			$scope.setMmodelOptions = function(elem)
			{
				return function(){
					$scope.currentmmodel.options=JSON.parse(elem.value);
					$rootScope.$storage.config.mmodeloptions[$scope.currentmmodel.name] = $scope.currentmmodel.options;
				};
			};
			$scope.showMmDialog = function($event)
			{
				hlpShared.showDialog($event,'#mmConfig',
				function(){$scope.currentmmodel.options+" ";},
				function(){});
			}
			$scope.dlDirOpenDialog = function()
			{
				remote.dialog.showOpenDialog({properties: ['openDirectory']}, (fileNames) =>
				{
					$rootScope.$storage.config.dldir = fileNames[0].replace(/\\/g, "/");
					$scope.$digest();
				});
			}
			$scope.getSelectedChapters=function(m)
			{
				var reslist=[];
				if(!m.chapters)
					return reslist;
				for(var i=0;i<m.chapters.length;i++)
					if(m.chapters[i].selected && m.chapters[i].version==$scope.currentm.curversion)
						reslist.push(m.chapters[i]);
				return reslist;
			}
			$scope.toggleChaExpand=function(currentm,chapter)
			{
				for(var i=0;i<currentm.chapters.length;i++)
					if(chapter.volnumb==currentm.chapters[i].volnumb && chapter.version==currentm.chapters[i].version)
						currentm.chapters[i].expand=!currentm.chapters[i].expand;
			}
			$scope.toggleChaSelection=function(currentm,chapter,isvol,$index,length)
			{
				if(!length||(($index + length)>currentm.chapters.length))
					length=currentm.chapters.length-$index;
				for(var i=$index; i<$index+length; i++)
				{
					if(chapter.volnumb==currentm.chapters[i].volnumb && chapter.version==currentm.chapters[i].version)
						if(!isvol)
							currentm.chapters[i].selected=!currentm.chapters[i].selected;
						else
							currentm.chapters[i].selected=chapter.volsel;
				}
				var volsel = false;
				var volchap;
				for(var i=0;i<currentm.chapters.length;i++)
				{
					if(chapter.volnumb==currentm.chapters[i].volnumb && chapter.version==currentm.chapters[i].version)
					{
						if(currentm.chapters[i].selected)
							volsel=true;
						if(currentm.chapters[i].newvol)
							volchap = currentm.chapters[i];
					}
				}
				volchap.volsel = volsel;
			}
		
		
			$scope.getMangas = async function(forcenew)
			{
				if($scope.currentmmodel.options.livesearch)
					return;
				
				$scope.currentmlist = {};
		
				for(var i=0;i<$scope.mlists.length;i++)
					if($scope.mlists[i].mmodelname == $scope.currentmmodel.name)
						$scope.currentmlist = $scope.mlists[i];
		
				if(forcenew||parseInt($scope.currentmlist.progress)!=100)
				{
					$scope.currentmlist.progress = 0;
					$scope.currentmlist.cont = [];
					$scope.currentmlist.mmodelname = $scope.currentmmodel.name;
					
					try
					{	
						$scope.currentmlist.cont = await $scope.currentmmodel.mextract({"$scope":$scope,"$http":$http,"$q":$q,"mmShared":mmShared});
						$scope.currentmlist.progress = 100;
						$scope.currentmlist.cont.sort($scope.hlpShared.sortByAttr('name'));
						
						if(!$scope.mlists.includes((mlist) => {return mlist == $scope.currentmlist.mmodelname}))
							$scope.mlists.push($scope.currentmlist);
						
						fs.writeFile(remote.app.getPath("userData")+"/emdl-mlists.json", JSON.stringify($scope.mlists), function(err) {
							if(err)
								console.warn({"desc":"failed to save the mlists to"+remote.app.getPath("userData")+"/emdl-mlists.json. new mlist data wont be available on restart","errmsg":err});
						});
					}
					catch(e)
					{
						$scope.currentmlist.progress = 100;
						console.error({"desc":"failed to extract the manga list for mmodel "+$scope.currentmlist.mmodelname,"method":"mextract","receiveddata":e});
					}
				}
				if(!$rootScope.$$phase)
					$scope.$digest();
			}
			$scope.getChapters = async function(m)
			{
				$scope.currentm = JSON.parse(JSON.stringify(m));
				$scope.currentm.name = $scope.currentm.name || "undefined";
				
				try
				{
					var chapters = await $scope.currentmmodel.cextract(m,{"$scope":$scope,"$http":$http,"$q":$q,"mmShared":mmShared});
		
					if(!chapters||chapters.length==0)
						throw new Error('chapters array is empty');
					
					chapters.sort(hlpShared.sortChapters);
					chapters[0].newvol = true;
					chapters[0].volsel = true;
					chapters[0].chadiv = true;
					chapters[0].selected = true;
					var k = 1;
					for(var i=1;i<chapters.length;i++)
					{
						chapters[i].selected = true;
						if(k%5==0)
							chapters[i].chadiv = true;
						if(chapters[i-1].volnumb!=chapters[i].volnumb || chapters[i-1].version!=chapters[i].version)
						{
							chapters[i].newvol = true;
							chapters[i].volsel = true;
							chapters[i].chadiv = true;
							k=0;
						}
						k++;
					}
		
					var versions = [];
					for(var i=0;i<chapters.length;i++)
						if (chapters[i].version&&versions.indexOf(chapters[i].version) == -1)
							versions.push(chapters[i].version);
		
					if(versions.length>=1)
					{
						versions.sort();
						$scope.currentm.curversion = versions[0];
						$scope.currentm.versions = versions;
					}
		
					$scope.currentm.chapters = chapters;
				}
				catch(e)
				{
					$scope.currentm.name = "failed to retrieve any chapter for your input";
					$scope.currentm.error = true;
					console.warn({"cause":e,"method":"cextract","input":m});
				}
				
				if(!$rootScope.$$phase)
					$scope.$digest();
			}
			$scope.getLiveSearchChapters = function(name)
			{
				if(!$scope.currentmmodel.options.livesearch)
					return;
				var m = {name:name||$rootScope.$storage.config.search, link:name||$rootScope.$storage.config.search};
				if($scope.currentmmodel.getMlink)
					m.link = $scope.currentmmodel.getMlink(m);
				$scope.getChapters(m);
			}
		
			//wrapper for child process execution
			$scope.getThreadResult = function(worker,input)
			{
				// console.log(input);
				return new Promise(function(resolve,reject)
				{
					worker.promiseQueue = worker.promiseQueue || [];
					worker.promiseQueue.push({resolve:resolve,reject:reject});
					worker.onmessage = function (ev) {
						// console.log(ev.data);
						if(ev.data.hasOwnProperty("debug"))
							console.log("debug", ...ev.data.debug);
						else
							if(ev.data.hasOwnProperty("error"))
								worker.promiseQueue.pop().reject(ev.data.error);
							else
								worker.promiseQueue.pop().resolve(ev.data);
					};
					worker.onerror = function (ev) {
						if(ev.code == "EPIPE")
							ev.message = "probably a syntax error in the worker";
						worker.promiseQueue.pop().reject(ev);
					};
					worker.postMessage(input);
				});
			}


			$scope.createDL = function()
			{
				if(!$scope.currentm||!$scope.currentmmodel)
					return;

				$scope.currentm.chapters = $scope.getSelectedChapters($scope.currentm);
				$scope.downloads.push(new Download($scope.currentm, $scope.currentmmodel, $rootScope.$storage.config));
				Download.checkState();
				$scope.currentm={};
			};
			$scope.toggleDlState = (dl) => dl.toggleState();
			$scope.removeDL = function($index)
			{
				$scope.downloads[$index].worker    && $scope.downloads[$index].worker.terminate();
				$scope.downloads[$index].imgworker && $scope.downloads[$index].imgworker.terminate();
				$scope.downloads.splice($index, 1);
				Download.checkState();
			};

			class Download
			{
				constructor(m, mmodel, config)
				{
					this.manga = JSON.parse(JSON.stringify(m));
					this.manga.chapters.forEach( (chapter) => chapter.skipped = true );
					this.requestPredict = {chapterCounter:0, pagesAccu:0, saveFailedCounter:0};
					this.mmodel = mmodel;
					this.mmodel.concurrencyLevel = this.mmodel.options.concurrencyLevel || "afterIextract";
					this.state = "queued";
					this.progress = "0.00";
					this.format = config.format;
					this.dlbasepath = config.dldir.replace(/\\/g,"/");
					this.dlreppath = config.dlreppath.replace(/\%MediaTitle\%/g, m.name.replace(/[\/\\]/g,'-')
																					   .replace(/[^A-Za-z0-9 \, \' \# \; \- \_ \( \) \+ \! \&]/g,'')
																					   .replace(/(^\s+|\s+$)/g, ''));
					this.reset();
				}

				reset()
				{
					this.i = 0;
					this.j = 0;
					if(this.keepProgress)
					{
						this.completed = this.completed - this.requestPredict.saveFailedCounter
						this.requested = this.completed;
					}
					else
					{
						this.requested = 0;
						this.completed = 0;			
						this.requestPredict = {chapterCounter:0, pagesAccu:0};
					}
					this.requestPredict.saveFailedCounter = 0;
					this.keepProgress = false;

					if(!this.worker||this.worker.child.killed)
						this.worker = new Worker(this.mmodel.workerpath);
					if(!this.imgworker||this.imgworker.child.killed)
						this.imgworker = new Worker($scope.jsbase+"save-worker.js");
				}

				start()
				{
					this.state = "running";
					this.reset();
					this.removeCompletedPages();
					Download.sort();

					var self = this;
					setTimeout(function(){Download.getPages(self)}, this.mmodel.latency);
				}

				removeCompletedPages()
				{
					for(var i=0;i<this.manga.chapters.length;i++)
						if(this.manga.chapters[i].pages)
							this.manga.chapters[i].pages = this.manga.chapters[i].pages.filter( (page) => {return page.skipped} );
					var skipCheck = this.manga.chapters.filter( (chapter) => {return chapter.skipped || (chapter.pages && chapter.pages.length);} );
					if(skipCheck.length == 0)
						this.skipped = false;
					else
						this.skipped = true;
				}

				async end()
				{
					if(this.state!="running" || this.completed!=this.requested || this.i<this.manga.chapters.length)
						return;
					
					this.state = "completed";
					this.worker.terminate();
					this.imgworker.terminate();
					Download.checkState();
			
					let dirpath;
					for(let chapter of this.manga.chapters)
						if(chapter.dlreppath)
						{
							dirpath =  this.dlbasepath + "/" + chapter.dlreppath.replace(this.dlbasepath,"").split("/").filter(Boolean)[0];
							break;
						}
						console.log(dirpath);
					this.removeCompletedPages();

					if(!dirpath)
					{
						console.warn({"desc":"all chapters of " + this.manga.name + " were skipped"});
						return;
					}
			
					var fileList = $scope.hlpShared.getFiles(dirpath).map(function(path){ return {path:path}; });
					console.info("download of " + this.manga.name + " finished; requested " + this.requested +
								" files and found " + fileList.length + " files in the folder " + dirpath);
			
					if(this.format == "cbz" || this.format == "pdf")
					{
						this.progress = "packing";
						var input = {"dirpath":dirpath, format:this.format, fileList: fileList};
						var archvworker = new Worker($scope.jsbase + "archv-worker.js");
						await $scope.getThreadResult(archvworker, input);
						archvworker.terminate();
						this.progress = "100.00";
						$scope.$digest();
					}

					$scope.$digest();
				}

				toggleState()
				{
					switch(this.state)
					{
						case 'running':
							this.state = 'paused';
							break;
						case 'queued':
							this.state = 'paused';
							break;
						case 'paused':
							this.state = 'queued';
							this.keepProgress = true;
							break;
						case 'completed':
							if(this.skipped)
							{
								this.state = "queued";
								this.keepProgress = true;
							}
							break;
					}
					Download.checkState();
				}


				static sort()
				{
					$scope.downloads.sort(
						function(a,b)
						{
							if(a.state=="running")
								return -1;
							if(a.state=="queued" && b.state!="running")
								return -1;
							if(a.state=="paused" && b.state!="running" && b.state!="queued")
								return -1;
							if(a.state=="completed" && b.state=="completed")
								return -1;
							return 1;
						}
					);
				}

				static checkState()
				{
					Download.sort();
					for(var i=0;i<$scope.downloads.length;i++)
						if($scope.downloads[i].state=="queued" && ($scope.downloads[i].mmodel.options.latency<=0 || ($scope.downloads[i].mmodel.options.latency>0 &&
								!$scope.downloads.some( download => {return download.mmodel.name==$scope.downloads[i].mmodel.name&&download.state=="running"}))))
							$scope.downloads[i].start();		
				}


				static errorLog(method, dl, i, j, error)
				{
					var volnumb = dl.manga.chapters[i] ? dl.manga.chapters[i].volnumb : undefined;
					var chanumb = dl.manga.chapters[i] ? dl.manga.chapters[i].chanumb : undefined;
					var pages = isNaN(j) ? "" : " p." + (j+1);
					console.warn({"method":method, "source":dl.manga.name+" v."+ volnumb +" ch."+ chanumb + pages, "cause":error, "input":dl.manga.chapters[i]});
				}
				static downloadNext(dl, level)
				{
					if(level == "chapter")
						setTimeout(function(){Download.getImages(dl);},dl.mmodel.options.latency);

					if(level == "pages")
						setTimeout(function()
						{
							dl.j++;
							Download.getImages(dl);
						}, Math.max(dl.mmodel.options.latency-(performance.now() - dl.timereq),0));
				}
				static async getPages(dl)
				{
					if(dl.manga.chapters[dl.i].pages)
					{
						Download.getImages(dl);
						return;
					}
	
					try
					{
						dl.manga.chapters[dl.i].skipped = true;
						var response = await $scope.getThreadResult(dl.worker,{"method":"pextract", "curchap": dl.manga.chapters[dl.i],"passthrough":dl.mmodel.passthrough});
						if(!response.data||response.data.length==0)
							throw new Error('pages array is empty');
						else
						{
							dl.manga.chapters[dl.i].skipped = false;
							dl.manga.chapters[dl.i].pages = response.data;
							dl.manga.chapters[dl.i].pages.forEach( (page) => page.skipped = true );
							dl.manga.chapters[dl.i].dlreppath = dl.dlreppath
																.replace(/\%Volume\%/g,dl.manga.chapters[dl.i].volnumb)
																.replace(/\%Chapter\%/g,dl.manga.chapters[dl.i].chanumb)
																.replace(/\%MModelName\%/g,dl.mmodel.name)
																.replace(/\%Version\%/g,dl.manga.chapters[dl.i].version)
																.replace(/\\/g,"/");
							dl.requestPredict.pagesAccu += dl.manga.chapters[dl.i].pages.length;
							dl.requestPredict.chapterCounter++;
						}
					}
					catch(e)
					{
						dl.manga.chapters[dl.i].pages = null;
						Download.errorLog("pextract", dl, dl.i, undefined, e);
					}
					finally
					{
						Download.downloadNext(dl, "chapter");
					}
					
				}

				static async getImages(dl)
				{
					if(dl.state!="running")
						return;
						
					if(!dl.manga.chapters[dl.i].pages||dl.j>=dl.manga.chapters[dl.i].pages.length)
					{
						dl.i++;
						dl.j = 0;
						if(dl.i<dl.manga.chapters.length)
							Download.getPages(dl);
						else
							dl.end();				
						return;
					}
					
					(async function (dli,dlj)
					{
						dl.mmodel.concurrencyLevel == "beforeIextract" && Download.downloadNext(dl, "pages");
						dl.timereq = performance.now();
						dl.manga.chapters[dl.i].pages[dl.j].skipped = true;

						var response = undefined;
						try
						{
							if(!dl.manga.chapters[dl.i].pages[dl.j].src)
							{
								response = await $scope.getThreadResult(dl.worker,{"method":"iextract", "curpage": dl.manga.chapters[dl.i].pages[dl.j],"passthrough":dl.mmodel.passthrough});
								if(!response.data || !response.data.src)
									throw new Error('got empty image source');	
							}
						}
						catch(e)
						{
							Download.errorLog("iextract", dl, dli, dlj, e);
							return;
						}
						finally
						{
							dl.mmodel.concurrencyLevel == "afterIextract" && Download.downloadNext(dl, "pages");
						}
						if(!dl.manga.chapters[dl.i].pages[dl.j].src)
							dl.manga.chapters[dl.i].pages[dl.j] = Object.assign(dl.manga.chapters[dl.i].pages[dl.j], response.data || {});
						dl.requested++;
								
						var watermarkConfig = null;
						if(dl.mmodel.options.removeWatermarks)
						{
							Function.prototype.toJSON = function() { return "("+this.toString()+")"; };
							watermarkConfig = JSON.stringify(dl.mmodel.watermarkConfig);
							Function.prototype.toJSON = undefined;
						}							
	
						try
						{
							var response = await $scope.getThreadResult(dl.imgworker,{"dlpath":dl.dlbasepath + "/" + dl.manga.chapters[dl.i].dlreppath.replace(/\%FileName\%/g,dl.manga.chapters[dl.i].pages[dl.j].numb),
																					  "data": dl.manga.chapters[dl.i].pages[dl.j], "i":dl.i,"j":dl.j,"watermarkConfig":watermarkConfig});
							dl.manga.chapters[response.input.i].pages[response.input.j].skipped = false;
						}
						catch(e)
						{	
							dl.requestPredict.saveFailedCounter++;
							Download.errorLog("mediaSaveWorker", dl, e.input.i, e.input.j, e);
						}
						finally
						{
							dl.completed++;
							dl.mmodel.concurrencyLevel == "afterMediaSave" && Download.downloadNext(dl, "pages");
							var guess = (dl.requestPredict.pagesAccu/dl.requestPredict.chapterCounter) * (dl.manga.chapters.length - dl.requestPredict.chapterCounter);
							dl.progress = (100*((dl.completed - dl.requestPredict.saveFailedCounter)/( dl.requestPredict.pagesAccu + guess))).toFixed(2);
							!$rootScope.$storage.reader.config.show && throttledDigest();
							dl.end();
						}
					})(dl.i,dl.j);
				}
			}
		
			//watchers
			$scope.$watch("currentmmodel",function(newValue, oldValue){
				if(!$scope.currentmmodel)
					return;
				
				$scope.hlpShared.currentmmodel = $scope.currentmmodel;
				$scope.currentm={};
				$scope.mlistContainerIndex = 0;
				
				if(oldValue)
					oldValue.cancelMlist = true;
				$scope.currentmmodel.cancelMlist = false;
				$scope.getMangas();
		
				$rootScope.$storage.config.currentmmodelname = $scope.currentmmodel.name;
			});	
		}
    };
}]);
	