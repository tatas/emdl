"use strict"; 

var importLazy = require('import-lazy')(require);
var fs = importLazy('fs-extra');
var request = importLazy('request');
var mime = importLazy('mime-types');
var Jimp = importLazy("jimp");

var phashPath = __dirname+"/node_modules/jimp/phash.js";
if(!require('electron-is-dev'))
	phashPath = __dirname+"/resources/app.asar/node_modules/jimp/phash.js";
var ImagePHash = importLazy(phashPath);

console.log = (...msg) => {postMessage({ "debug" : msg });}

var removeWatermarks = function(ev,body)
{
	Jimp.read(body, function (err, image)
	{
		var hashes = eval(eval(ev.data.watermarkConfig))(image);

		for(var i=0;i<hashes.length;i++)
		{
			try
			{
				var hash = image.clone().crop(hashes[i].area.x,hashes[i].area.y,hashes[i].area.w,hashes[i].area.h).hash();
				var found = false;
				for(var k=0;k<hashes[i].hash.length;k++)
					if(new ImagePHash().distance(hash,hashes[i].hash[k])<0.1)
					{
						hashes[i].removeFunction(image);
						found = true;
						break;
					}
				if(found)
					break;
			}catch(e){}
		}
		image.write(ev.data.dlpath,()=>{postMessage({ "success" : true, "input": ev.data });});
	});
}
									
									
var processresp = function(res,ev,reqcounter,getFct,cust)
{
	try
	{
		var datatype = null;
		var mismatch = false;
		if(res)
		{
			datatype = mime.extension(res.headers["content-type"]);
			if(datatype == "jpeg")
				datatype = "jpg";

			if(res.headers["content-length"]&&res.body&&(res.headers["content-length"]!=res.body.length))
				mismatch = true;
		}
		if((datatype && datatype != "html")&&!mismatch)
		{
			ev.data.dlpath = ev.data.dlpath.replace(/\%DataType\%/g,datatype);
			cust();
		}
		else
		{
			if(reqcounter<10)
				getFct(ev,reqcounter+1);
			else
			{	
				if(ev && ev.data)
					postMessage({ "error" : { desc: "save-worker: 10 failed attempts to receive an image from "+ev.data.data.src+" | image will be skipped",
									contentLengthMismatch:mismatch, input:ev.data, response:res} });
				else
					postMessage({ "error" : { desc: "save-worker: 10 failed attempts to receive an image from undefined | image will be skipped",
									contentLengthMismatch:mismatch, input:ev.data, response:res} });
			}
		}
	}
	catch(err)
	{
		postMessage({ "error" : {desc:"save-worker: error processing the response from request with the url: " + ev.data.data.src, errmsg:err.stack, response:body, input:ev.data}});
	}
}

var getRequestCfg = function(ev)
{
	return Object.assign({url : ev.data.data.src, encoding: null, timeout:60000,headers: {"User-Agent": "Mozilla/5.0"}}, ev.data.data.requestCfg || {});
}
var getImage = function (ev,reqcounter)
{
	request.get(getRequestCfg(ev), (err, res, body) => {
		processresp(res,ev,reqcounter,getImage,function()
		{
			if(ev.data.watermarkConfig)
				removeWatermarks(ev,body);
			else
				fs.writeFile(ev.data.dlpath,body, "binary",
				function (err)
				{
					if(err)
						postMessage({ "error" : {desc:"save-worker: error creating file " + ev.data.dlpath, errmsg:err, input:ev.data}});
					else
						postMessage({ "success" : true, "input": ev.data });
				});
		});
	});	
}

var getVideo = function (ev,reqcounter)
{
	var stream = request.get(getRequestCfg(ev));
	stream.on('response', function(res)
	{
		processresp(res,ev,reqcounter,getVideo,function()
		{
			var writable = fs.createWriteStream(ev.data.dlpath);
			stream.pipe(writable);
		});
	});
	stream.on('end', function () {
		postMessage({ "success" : true, "input": ev.data });
	});
}

var start = (async function(ev)
{
	var dirpath = ev.data.dlpath;
	dirpath = ev.data.dlpath.substring(0,Math.max(dirpath.lastIndexOf("/"),dirpath.lastIndexOf("\\"))+1);
	await fs.ensureDir(dirpath);
	if(ev.data.data.filestream)
		getVideo(ev,0);
	else
		getImage(ev,0);
});

onmessage = function (ev)
{
	start(ev);
};