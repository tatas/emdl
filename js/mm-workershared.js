'use strict';

var importLazy = require('import-lazy')(require);
var request = importLazy('request');
var cheerio = importLazy('cheerio');
var smartCircular = require('smart-circular');

var requestTimeout = 60000;

Function.prototype.toJSON = function() { return "("+this.toString()+")"; };
console.log = (...msg) => {
	postMessage({ "debug" : msg.map( (elem) =>  typeof elem == "object" ? smartCircular(elem) : elem)	});
}
console.error = (...msg) => {
	postMessage({ "error" : msg.map( (elem) =>  typeof elem == "object" ? smartCircular(elem) : elem)	});
}
console.data = (msg) => {postMessage({ "data" : msg });}


var workershared = {};
workershared.zeropad = function(num, size)
{
	var s = num+"";
	while (s.length < size) s = "0" + s;
	return s;
}	

workershared.commonPextract = function(options)
{
	if(options.debug)
		console.log({"method":"commonPextract","input":options});
	
	request(Object.assign({timeout:requestTimeout, gzip: true}, options.requestCfg),
	function (error, response, body)
	{
		if(options.debug)
			console.log({"method":"commonPextract","serverResponse":response});
		try
		{
			if(error)
				throw error;
			var pages = [];
			var $ = cheerio.load(body);

			if(options.selectFct)
			{	
				var htmllist = options.selectFct($);
				htmllist.each(function(i, elem)
				{
					var pushele = options.elementFct($(this),i);
					if(pushele)
					{	
						if(!isNaN(parseFloat(pushele.numb)))
							pushele.numb = workershared.zeropad(pushele.numb,4);
						pages.push(pushele);
					}
				});
			}
			else
			{
				var pushelems = options.elementFct($);
				for(let pushelem of pushelems)
				{	
					if(!isNaN(parseFloat(pushelem.numb)))
						pushelem.numb = workershared.zeropad(pushelem.numb,4);
					pages.push(pushelem);
				}
			}
			
			if(options.debug)
				console.log({"method":"commonPextract","pages":pages});
			postMessage({ "data" : pages });
		}
		catch(e)
		{
			postMessage({ "error" : e.stack });
		}
	});
}

workershared.commonIextract = function(options)
{
	if(options.debug)
		console.log({"method":"commonIextract","input":options});
	request(Object.assign({timeout:requestTimeout, gzip: true}, options.requestCfg),
	function (error, response, body)
	{
		if(options.debug)
			console.log({"method":"commonIextract","serverResponse":response});
		try
		{
			if(error)
				throw error;
			var $ = cheerio.load(body);
			var result = options.elementFct($);
			if(options.debug)
				console.log({"method":"commonIextract","imageSource":result});
			postMessage({ "data" : result});
		}
		catch(e)
		{
			postMessage({ "error" : e.stack });
		}
	});
}

module.exports = workershared;