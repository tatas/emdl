appmod.factory('hlpShared',['$rootScope', '$mdDialog', 'themeProvider', '$mdTheming',

function ($rootScope, $mdDialog, themeProvider, $mdTheming)
{
	
	var factory = {};

	factory.addMissingProps = function(reference,config)
	{
		for(var prop in reference)
		{
			if(!config.hasOwnProperty(prop))
				config[prop] = reference[prop];
			if(typeof reference[prop] == "object" && prop != "styleContainer" && prop != "styleImage")
			factory.addMissingProps(reference[prop],config[prop]);
		}
	}
	//color theme config
	factory.colorPalettes = ["amber", "blue", "blue-grey", "brown", "cyan", "deep-orange", "deep-purple", "green", "grey", "indigo", "light-blue", "light-green", "lime", "orange", "pink", "purple", "red", "teal", "yellow"];
	$rootScope.theme = "default";
	factory.updateColorTheme = function(firstRun)
	{
		if(!firstRun)
			$rootScope.theme+="1";
		themeProvider.theme($rootScope.theme)
			.primaryPalette($rootScope.$storage.config.colorTheme.primaryPalette)
			.accentPalette($rootScope.$storage.config.colorTheme.accentPalette)
			.warnPalette($rootScope.$storage.config.colorTheme.warnPalette)
			.backgroundPalette($rootScope.$storage.config.colorTheme.backgroundPalette)
			.dark($rootScope.$storage.config.colorTheme.darkTheme);
		$mdTheming.generateTheme($rootScope.theme);
		themeProvider.setDefaultTheme($rootScope.theme);
	}
	factory.setBackgroundThrottling = () => currentWindow.webContents.setBackgroundThrottling($rootScope.$storage.config.enableBackgroundThrottling);

	factory.throttle = function(callback, delay) {
		var timeout
			,last_run = 0;
		return function () {
			if (timeout) {
				return;
			}
			var elapsed = (+new Date()) - last_run
				,context = this
				,args = arguments
				,run_callback = function() {
					last_run = +new Date();
					timeout = false;
					callback.apply(context, args);
				}
				;
			if (elapsed >= delay) {
				run_callback();
			}
			else {
				timeout = setTimeout(run_callback, delay);
			}
		};
	}


	factory.zeropad = function(num, size)
	{
		var s = num+"";
		while (s.length < size) s = "0" + s;
		return s;
	}

	factory.openWebConsole = currentWindow.openDevTools;
	
	factory.sortByAttr = function(attr)
	{
		var funct = function(a, b) {
			if(a[attr].toLowerCase() > b[attr].toLowerCase())
				return 1;
			else
				return -1;
		};
		return funct;	
	}
	var sortFilesHelper = function(a, b)
	{
		var aarr=a.toLowerCase().split("/");
		var barr=b.toLowerCase().split("/");

		if(aarr.length<barr.length)
			return -1;
		if(aarr.length>barr.length)
			return 1;
		
		for(var i=0;i<aarr.length;i++)
		{
			if(aarr[i]<barr[i])
				return -1;
			if(aarr[i]>barr[i])
				return 1;
		}
		return 0;
	};
	factory.sortFiles = function(a, b)
	{
		var sortVal = sortFilesHelper(a.path, b.path);
		if(sortVal)
			return sortVal;
		return sortFilesHelper(a.zipPath, b.zipPath);
	};
	factory.sortChapters = function(a, b)
	{
		if(a.version > b.version)
			return 1;
		if(a.version < b.version)
			return -1;
		if(a.volnumb > b.volnumb)
			return 1;
		if(a.volnumb < b.volnumb)
			return -1;
		if(a.chanumb > b.chanumb)
			return 1;
		return -1;
	};
	
	factory.getFiles = function (dir, files_){
		files_ = files_ || [];
		var files = fs.readdirSync(dir);
		for (var i in files){
			var name = dir + '/' + files[i];
			if (fs.statSync(name).isDirectory()){
				factory.getFiles(name, files_);
			} else {
				files_.push(name);
			}
		}
		return files_;
	}
	factory.getFilesNonRec = function (dir){
		var fileList = [];			
		var files = fs.readdirSync(dir);
		for(var i in files){
			if (!files.hasOwnProperty(i)) continue;
				fileList.push({name:files[i],path:dir+'/'+files[i]});
		}
		return fileList;
	}
	factory.showDialog = function(ev,selector,onComplete,onRemoving)
	{
		return $mdDialog.show(
		{
			contentElement: selector,
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose: true,
			onComplete:()=>
			{
				//workaround for electron 4 bug: mouse wheel scroll doesnt work for dialog and textarea autogrow init
				window.dispatchEvent(new Event("resize"));
				onComplete && onComplete();
			},
			onRemoving:onRemoving
		});
	};
	factory.valMdInput = function(elem, funct)
	{
		elem.classList.remove("md-input-invalid");
		try{funct();}
		catch(e){elem.classList.add("md-input-invalid");}
	}
	factory.openExternal = electron.shell.openExternal;

	factory.textInputMenu = [];
	for(let item of ["Cut","Copy","Paste","Delete"])
		factory.textInputMenu.push(
		[item, function ($itemScope, $event, modelValue, text, $li)
		{
			$event.target.focus();
			currentWindow.webContents[item.toLowerCase()]();
		}]);
	factory.openInExplorerMenu = 
	[
		['Open in explorer', function ($itemScope, $event, modelValue, text, $li) {
			electron.shell.openExternal(modelValue.replace(/\\/g, "/"));
		}],
		null
	].concat(factory.textInputMenu);
	
	
	
	return factory;
}]
);