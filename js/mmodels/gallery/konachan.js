module.exports = 
{
	name:"konachan",
	options:
	{
		type:"gallery",
		livesearch: true,
		latency:0,
		custom:
		{
			comment:"Use imagetype 'highres' for a higher resolution or 'lowres' for a lower resolution.",
			imagetype:"lowres"
		}
	},
	passthrough:{},	
	getMlink:function(m){return "http://konachan.com/post?tags="+m.name+"&page="},
	cextract:function(m,ng)
	{	
		this.passthrough.imagetype = this.options.custom.imagetype;
		return ng.mmShared.commonCextract(
		{
			requestCfg:{url:m.link+1},
			selectFct: function(dom)
			{
				var maxcha = 1;
				var chapts = [];
				var elems = dom.getElementsByClassName("pagination")[0];
				if(elems)
				{	
					elems = elems.getElementsByTagName("a");
					maxcha = parseInt(elems[elems.length-2].innerText);
				}
				if(dom.getElementsByClassName("preview").length>0)
					for(var i=0;i<maxcha;i++)
					{	
						chapts.push({volnumb:parseInt(i/100)+1,chanumb:(i+1),link:m.link+(i+1)});
					}
				return chapts;
			}
		});
	}
};
