var mmworkershared = require('./../../../js/mm-workershared.js');

onmessage = function (ev)
{
	if(ev.data.method == "pextract")
	{
		mmworkershared.commonPextract(
		{
			requestCfg:{ url:ev.data.curchap.link },
			selectFct:function($){return $(".thumb");},
			elementFct:function(elem,i){return {numb:i+1,link:"https://gelbooru.com/"+elem.find("a").attr('href')};}
		});
	}

	if(ev.data.method == "iextract")
	{
		mmworkershared.commonIextract(
		{
			requestCfg:{ url:ev.data.curpage.link },
			elementFct:function($)
			{
				var filestream = false;
				if(ev.data.passthrough.imagetype=="highres")
				{
					if(!$('#image').attr('src'))
						filestream = true;
					return {src:$("meta[property='og:image']").prop("content"),filestream:filestream};
				}
				else
				{
					var src=$('#image').attr('src');
					if(!src)
					{	
						src=$("meta[property='og:image']").prop("content");
						filestream = true;
					}
					if(src[0]=="/"&&src[1]=="/")
						src = "http:" + src;
					return {src:src,filestream:filestream};
				}
			}
		});
	}	
};