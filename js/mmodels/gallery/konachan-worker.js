var mmworkershared = require('./../../../js/mm-workershared.js');

onmessage = function (ev)
{
	if(ev.data.method == "pextract")
	{
		mmworkershared.commonPextract(
		{
			requestCfg:{ url:ev.data.curchap.link },
			selectFct:function($){return $(".thumb");},
			elementFct:function(elem,i){return {numb:i+1,link:"http://konachan.com"+elem.attr('href')};}	
		});
	}

	if(ev.data.method == "iextract")
	{
		mmworkershared.commonIextract(
		{
			requestCfg:{ url:ev.data.curpage.link },
			elementFct:function($)
			{
				if(ev.data.passthrough.imagetype=="highres")
					return {src:$('#highres').attr('href')};
				else
					return {src:$('#image').attr('src')};
			}
		});
	}	
};