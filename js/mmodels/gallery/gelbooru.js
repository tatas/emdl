module.exports = 
{
	name:"gelbooru",
	options:
	{
		type:"gallery",
		livesearch: true,
		latency:0,
		custom:
		{
			comment:"Use imagetype 'highres' for a higher resolution or 'lowres' for a lower resolution.",
			imagetype:"highres"
		}
	},
	passthrough:{},
	getMlink:function(m){return "https://gelbooru.com/index.php?page=post&s=list&tags="+m.name+"&pid=";},
	cextract:function(m,ng)
	{	
		this.passthrough.imagetype = this.options.custom.imagetype;

		return ng.mmShared.commonCextract(
		{
			requestCfg:{url:m.link+"19992"},
			selectFct: function(dom)
			{
				var chapts=[];
				var last = dom.getElementById("paginator").getElementsByTagName("a");
				if(!last||last.length==0)
					return chapts;
				if(last.length==1)
					last=1;
				else
				{
					for(var i=last.length-1;i>0;i--)
						if(!isNaN(parseInt(last[i].innerText)))
						{	
							last=parseInt(last[i].innerText);
							break;
						}
				}
				for(var i=0;i<last;i++)	
					chapts.push({volnumb:parseInt(i/100)+1,chanumb:(i+1),link:m.link+(i*42)});
				return chapts;
			}
		});
	}
};
