module.exports = 
{
	name:"comicextra",
	options:
	{
		type:"comic",
		latency:0
	},
	mextract:function(ng)
	{
		return ng.mmShared.commonMextract(
		{
			requestCfg:{url:"http://www.comicextra.com/comic-list"},
			selectFct: function(dom){return dom.querySelectorAll(".series-col a");},
			elementFct: function(elem)
			{
				if(elem.href!="http://www.comicextra.com/")
					return {"name":elem.innerText,"link":elem.href};
				else
					return null;
			}
		});
	},
	cextract:function(m,ng)
	{	
		return ng.mmShared.commonCextract(
		{
			requestCfg:{url:m.link},
			selectFct: function(dom)
			{
				var elems = dom.getElementById("list").getElementsByTagName("a");
				return [].slice.call(elems).reverse();
			},
			elementFct: function(elem)
			{
				var chanumb = /#((.)+)/g.exec(elem.innerText)[1];	
				return {"volnumb":1,"chanumb":chanumb,"link":elem.href};
			}
		});
	}
};
