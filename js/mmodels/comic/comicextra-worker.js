var mmworkershared = require('./../../../js/mm-workershared.js');

onmessage = function (ev)
{
	if(ev.data.method == "pextract")
	{
		mmworkershared.commonPextract(
		{
			requestCfg:{ url:ev.data.curchap.link+"/full", headers:{"User-Agent": "Mozilla/5.0"} },
			selectFct:function($){return $(".chapter-container img");},
			elementFct:function(elem,i){return {numb:i+1,src:elem.attr('src')};}	
		});
	}	
};