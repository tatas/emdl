module.exports = 
{
	name:"readcomicsonline",
	options:
	{
		type:"comic",
		latency:0
	},
	mextract:function(ng)
	{
		return ng.mmShared.commonMextract(
		{
			requestCfg:{url:"http://readcomicsonline.ru/changeMangaList?type=text"},
			selectFct: function(dom){return dom.getElementsByTagName("a");},
			elementFct: function(elem)
			{
				if(elem.href)
					return {"name":elem.getElementsByTagName("h6")[0].innerText,"link":elem.href};
				else
					return false;
			}
		});
	},
	cextract:function(m,ng)
	{	
		return ng.mmShared.commonCextract(
		{
			requestCfg:{url:m.link},
			selectFct: function(dom)
			{
				var elems = dom.getElementsByClassName("chapters")[0].getElementsByTagName("a");
				return [].slice.call(elems).reverse();
			},
			elementFct: function(elem)
			{
				var splarr = elem.innerText.split("#").filter(Boolean);					
				return {"volnumb":1,"chanumb":splarr[splarr.length-1],"link":elem.href};
			}
		});
	}
};
