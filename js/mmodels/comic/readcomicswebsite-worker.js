var mmworkershared = require('./../../../js/mm-workershared.js');

onmessage = function (ev)
{
	if(ev.data.method == "pextract")
	{
		mmworkershared.commonPextract(
		{
			requestCfg:{ url:ev.data.curchap.link },
			selectFct:function($){return $("#page-list option");},
			elementFct:function(elem,i){return {numb:i+1,link:ev.data.curchap.link+"/"+elem.attr('value')};}	
		});
	}

	if(ev.data.method == "iextract")
	{
		mmworkershared.commonIextract(
		{
			requestCfg:{ url:ev.data.curpage.link },
			elementFct:function($){return {src:$("#ppp img").attr("src")};}
		});
	}	
};