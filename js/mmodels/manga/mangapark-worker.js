var mmworkershared = require('./../../../js/mm-workershared.js');

onmessage = function (ev)
{
	if(ev.data.method == "pextract")
	{
		mmworkershared.commonPextract(
		{
			requestCfg:{ url:ev.data.curchap.link },
			elementFct:function($)
			{
				var imgArray = JSON.parse($.html().split("var images = ")[1].split(";")[0]);
				var pagesArray = [];
				for(let i=0; i<imgArray.length; i++)
					pagesArray.push({numb:i+1,src:imgArray[i]});
				return pagesArray;
			}
		});
	}
};