module.exports = 
{
	name:"readmanga",
	options:
	{
		type:"manga",
		latency:0
	},
	mextract:function(ng)
	{
		var reqcharlist = " abcdefghijklmnopqrstuvwxyz";
		return ng.mmShared.commonMextract(
		{
			requestCfg:{url:function(i){return "http://www.readmanga.today/manga-list/"+reqcharlist[i];}},
			options:
			{
				url:function(i){return "http://www.readmanga.today/manga-list/"+reqcharlist[i];},
				gesiter:reqcharlist.length
			},
			selectFct: function(dom){return dom.getElementsByClassName("manga-item");},
			elementFct: function(elem)
			{
				var aelem = elem.getElementsByTagName("a")[0];
				return {"name":aelem.innerText,"link":aelem.href};
			},
		});
	},
	cextract:function(m,ng)
	{	
		return ng.mmShared.commonCextract(
		{
			requestCfg:{url:m.link},
			selectFct: function(dom)
			{
				var elems = dom.getElementsByClassName("chp_lst")[0].getElementsByTagName("a");
				return [].slice.call(elems).reverse();
			},
			elementFct: function(elem)
			{
				var splarr = elem.href.split("/").filter(Boolean);					
				return {"volnumb":1,"chanumb":splarr[splarr.length-1],"link":elem.href};
			}
		});
	}
};
