module.exports = 
{
	name:"webtoons.com",
	options:
	{
		type:"manga",
		latency:0
	},
	mextract:function(ng)
	{
		return ng.mmShared.commonMextract(
		{
			requestCfg:{url:function(i){return "https://www.webtoons.com/en/challenge/list?genreTab=ALL&sortOrder=LIKEIT&page="+(i+1);}},
			options:
			{
				gesiter:500,
				latency:500,
				cancelOnEmpty: true
			},
			selectFct: function(dom){
				if(dom.getElementsByClassName("paginate")[0].getElementsByTagName("a").length === 0)
					return null;
				else
					return dom.getElementsByClassName("challenge_lst")[0].getElementsByClassName("challenge_item");},
			elementFct: function(elem)
			{
				return {name:elem.getElementsByClassName("subj")[0].innerText.trim(),link:elem.href};
			},
		});
	},
	cextract: async function(m,ng)
	{
		var requestLink = new DOMParser().parseFromString((await importLazy('request-promise')(m.link)), "text/html").getElementById("_btnEpisode").href;	
		return ng.mmShared.commonCextract(
		{
			requestCfg:{url:requestLink},
			selectFct: function(dom)
			{
				return dom.getElementsByClassName("episode_cont")[0].getElementsByTagName("li");
			},
			elementFct: function(elem)
			{
				return {"volnumb":null,"chanumb":null ,"link":elem.getElementsByTagName("a")[0].href};
			}
		});
	}
};
