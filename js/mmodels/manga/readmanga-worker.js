var mmworkershared = require('./../../../js/mm-workershared.js');

onmessage = function (ev)
{
	if(ev.data.method == "pextract")
	{
		mmworkershared.commonPextract(
		{
			requestCfg:{ url:ev.data.curchap.link+"/all-pages" },
			selectFct:function($){return $(".img-responsive");},
			elementFct:function(elem,i){return {numb:i+1,src:elem.attr("src")};}
		});
	}
};