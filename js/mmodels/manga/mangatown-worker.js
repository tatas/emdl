var mmworkershared = require('./../../../js/mm-workershared.js');

onmessage = function (ev)
{
	if(ev.data.method == "pextract")
	{
		var urltmpl = ev.data.curchap.link;
		mmworkershared.commonPextract(
		{
			requestCfg:{ url:ev.data.curchap.link },
			selectFct:function($){return $(".manga_read_footer .page_select select option");},
			elementFct:function(elem,i)
			{
				var numb = parseFloat(elem.text());
				if(numb)
					return {numb:numb,link:i==0?urltmpl:urltmpl+numb+".html"};
				else
					return null;
			}
		});
	}

	if(ev.data.method == "iextract")
	{
		mmworkershared.commonIextract(
		{
			requestCfg:{ url:ev.data.curpage.link },
			elementFct:function($){return {src:$('#image').attr('src')};}
		});
	}	
};