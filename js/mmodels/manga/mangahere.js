module.exports = 
{
	name:"mangahere",
	options:
	{
		type:"manga",
		latency:400
	},
	mextract:function(ng)
	{
		return ng.mmShared.commonMextract(
		{
			requestCfg:{url:"http://www.mangahere.co/mangalist/"},
			selectFct: function(dom){return dom.getElementsByClassName("manga_info");},
			elementFct: function(elem){return {"name":elem.innerText,"link":"http:"+/(href=\")([^"]+)/g.exec(elem.outerHTML)[2]};},
		});
	},
	cextract:function(m,ng)
	{	
		return ng.mmShared.commonCextract(
		{
			requestCfg:{url:m.link},
			selectFct: function(dom)
			{
				var elems = dom.getElementsByClassName("detail_list")[0].getElementsByTagName("ul")[0].getElementsByTagName("a");
				return [].slice.call(elems).reverse();
			},
			elementFct: function(elem)
			{
				elem.href = "http:"+/(href=\")([^"]+)/g.exec(elem.outerHTML)[2];
				var chanumb = /(c)(\d+(\.?\d*)*)/g.exec(elem.href)[2];	
				var volregex = /(v)(\d+)/g.exec(elem.href);
				var volnumb = volregex?volregex[2]:null;
							
				return {"volnumb":volnumb,"chanumb":chanumb,"link":elem.href};
			}
		});
	}
};
