var mmworkershared = require('./../../../js/mm-workershared.js');

onmessage = function (ev)
{
	if(ev.data.method == "pextract")
	{
		var urltmpl = ev.data.curchap.link.match(/^((?!\d+\.html).)*/)[0];
		mmworkershared.commonPextract(
		{
			requestCfg:{ url:ev.data.curchap.link },
			selectFct:function($){return $('.readpage_top .go_page.clearfix select.wid60 option');},
			elementFct:function(elem,i)
			{
				var numb = parseFloat(elem.text());
				if(numb)
					return {numb:numb,link:urltmpl+numb+".html"};
				else
					return null;
			}
		});
	}

	if(ev.data.method == "iextract")
	{
		mmworkershared.commonIextract(
		{
			requestCfg:{ url:ev.data.curpage.link },
			elementFct:function($){return {src:$('#image').attr('src')};}
		});
	}
};


