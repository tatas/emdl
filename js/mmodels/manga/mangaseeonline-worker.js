var mmworkershared = require('./../../../js/mm-workershared.js');

onmessage = function (ev)
{
	if(ev.data.method == "pextract")
	{
		var urltmpl = ev.data.curchap.link.match(/^((?!\d+\.html).)*/)[0];
		mmworkershared.commonPextract(
		{
			requestCfg:{ url:ev.data.curchap.link },
			selectFct:function($){return $('.PageSelect.hidden-xs option');},
			elementFct:function(elem,i)
			{
				var numb = elem.attr('value');
				if(numb)
					return {numb:numb,link:urltmpl+numb+".html"};
				else
					return null;
			}
		});
	}

	if(ev.data.method == "iextract")
	{
		mmworkershared.commonIextract(
		{
			requestCfg:{ url:ev.data.curpage.link },
			elementFct:function($){return {src:$('.CurImage').attr('src')};}
		});
	}	
};