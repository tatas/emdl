module.exports = 
{
	name:"mangapark",
	options:
	{
		type:"manga",
		latency:0
	},
	mextract:function(ng)
	{
		return ng.mmShared.commonMextract(
		{
			requestCfg:{url:function(i){return "https://mangapark.org/browse?order_by=a-z&page="+(i+1);}},
			options:
			{
				gesiter:1500,
				latency:1,
				getmaxpage:function(dom)
				{
					var elems =  dom.getElementsByClassName("pagination")[0].getElementsByClassName("page-link");
					return (/(href=\")([^"]+)/g.exec(elems[elems.length-2].outerHTML)[2]).split("page=")[1];
				}
			},
			selectFct: function(dom){return dom.getElementById("browse").children;},
			elementFct: function(elem)
			{
				return {name:elem.getElementsByTagName("h6")[0].innerText,
					    link:"https://mangapark.org"+/(href=\")([^"]+)/g.exec(elem.getElementsByTagName("a")[0].outerHTML)[2]};
			},
		});
	},
	cextract:function(m,ng)
	{	
		return ng.mmShared.commonCextract(
		{
			requestCfg:{url:m.link},
			selectFct: (dom) => dom.getElementsByClassName("card"),
			elementFct: function(elem)
			{
				var chapters = [];
				
				let versionName = elem.getElementsByTagName("a")[0].innerText;
				let chaptArray = Array.from(elem.getElementsByClassName("card-body")[0].getElementsByTagName("a")).reverse();
				for(let chaptElem of chaptArray)
				{	
					var charegex = /(ch.)[^\d]*(\d+(\.?\d*)*)/g.exec(chaptElem.innerText.toLowerCase());
					var chanumb = charegex?charegex[2]:null;
					var volregex = /(vol\.)(\d+)/g.exec(chaptElem.innerText.toLowerCase());
					var volnumb = volregex?volregex[2]:null;
					
					chapters.push({volnumb:volnumb,chanumb:chanumb,version:versionName,
							link:"https://mangapark.org"+/(href=\")([^"]+)/g.exec(chaptElem.outerHTML)[2]});
				}	
				return chapters;
			},
		});
	}
};
