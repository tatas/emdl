module.exports = 
{
	name:"mangaseeonline",
	options:
	{
		type:"manga",
		latency:0
	},
	mextract:function(ng)
	{
		return ng.mmShared.commonMextract(
		{
			requestCfg:{url:"http://mangaseeonline.us/directory/"},
			selectFct: function(dom){return dom.getElementsByClassName("ttip");},
			elementFct: function(elem){return {"name":elem.innerText,link:"http://mangaseeonline.us"+/(href=\")([^"]+)/g.exec(elem.outerHTML)[2]};}
		});
	},
	cextract:function(m,ng)
	{	
		return ng.mmShared.commonCextract(
		{
			requestCfg:{url:m.link},
			selectFct: function(dom)
			{
				var elems = dom.getElementsByClassName("list chapter-list")[0].getElementsByTagName("a");
				return [].slice.call(elems).reverse();
			},
			elementFct: function(elem)
			{
				var chanumb = elem.outerHTML.match(/chapter=\".*?\"/)[0].replace("chapter=\"","").replace("\"","");
				return {"volnumb":1,"chanumb":chanumb,"link":"http://mangaseeonline.us"+/(href=\")([^"]+)/g.exec(elem.outerHTML)[2]};
			}
		});
	}
};
