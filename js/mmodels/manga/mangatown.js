module.exports = 
{
	name:"mangatown",
	options:
	{
		type:"manga",
		latency:400
	},
	mextract:function(ng)
	{
		return ng.mmShared.commonMextract(
		{
			requestCfg:{url:function(i){return "http://www.mangatown.com/directory/0-0-0-0-0-0/"+(i+1)+".htm?name.az";}},
			options:
			{
				gesiter:1500,
				getmaxpage:function(dom){return dom.getElementsByClassName("next-page")[0].getElementsByTagName("select")[0][0].innerText.replace("1/","");}
			},
			selectFct: function(dom){return dom.getElementsByClassName("manga_cover");},
			elementFct: function(elem){return {name:elem.title,link:"http:"+/(href=\")([^"]+)/g.exec(elem.outerHTML)[2]};},
		});
	},
	cextract:function(m,ng)
	{	
		return ng.mmShared.commonCextract(
		{
			requestCfg:{url:m.link},
			selectFct: function(dom)
			{
				var elems = dom.getElementsByClassName("chapter_list")[0].getElementsByTagName("a");
				return [].slice.call(elems).reverse();
			},
			elementFct: function(elem)
			{
				elem.href = "http:"+/(href=\")([^"]+)/g.exec(elem.outerHTML)[2];
				if(!elem.href)
					return;
				var chanumb = /c(\d+[\.?\d*]*)\//g.exec(elem.href)[1]; 
				var volregex = /(Vol\ )(\d+(\.?\d*)*)/g.exec(elem.href); 
				var volnumb = volregex?volregex[2]:null;
							
				return {"volnumb":volnumb,"chanumb":chanumb,"link":elem.href};
			}
		});
	}
};
