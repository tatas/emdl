module.exports = 
{
	name:"manganel",
	options:
	{
		type:"manga",
		latency:0
	},
	mextract:function(ng)
	{
		return ng.mmShared.commonMextract(
		{
			requestCfg:{url:function(i){return "https://manganelo.com/manga_list?type=new&category=all&alpha=all&page="+(i+1);}},
			options:
			{
				gesiter:1500,
				latency:1,
				getmaxpage:function(dom){return dom.getElementsByClassName("phan-trang")[0].getElementsByClassName("page page-blue")[1].href.match(/\&page\=\d+/g)[0].replace("&page=","");}
			},
			selectFct: function(dom){return dom.getElementsByClassName("list-truyen-item-wrap");},
			elementFct: function(elem)
			{
				var ael = elem.getElementsByTagName("h3")[0].getElementsByTagName("a")[0];
				return {name:ael.innerText,link:ael.href};
			},
		});
	},
	cextract:function(m,ng)
	{	
		return ng.mmShared.commonCextract(
		{
			requestCfg:{url:m.link},
			selectFct: function(dom)
			{
				var elems = dom.getElementsByClassName("chapter-list")[0].getElementsByTagName("a");
				return [].slice.call(elems).reverse();
			},
			elementFct: function(elem)
			{
				var execRegex = /[cC]hapter \D*(\d+[\.?\d*]*)/g.exec(elem.innerText);
				var chanumb = execRegex ? execRegex[1] : undefined; 
				var volregex = /([vV]ol\.)(\d+(\.?\d*)*)/g.exec(elem.innerText); 
				var volnumb = volregex ? volregex[2] : undefined;
							
				return {"volnumb":volnumb,"chanumb":chanumb,"link":elem.href};
			}
		});
	}
};
