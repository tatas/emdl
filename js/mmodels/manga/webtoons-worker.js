var mmworkershared = require('./../../../js/mm-workershared.js');

onmessage = function (ev)
{
	if(ev.data.method == "pextract")
	{
		mmworkershared.commonPextract(
		{
			requestCfg:{ url:ev.data.curchap.link },
			selectFct:function($){return $("#_imageList img");},
			elementFct:function(elem,i)
			{
				var page =
				{
					numb:i+1,
					src:elem.attr('data-url'),
					requestCfg:
					{
						headers:
						{
							"User-Agent": "Mozilla/5.0",
							"Host": "webtoon-phinf.pstatic.net",
							"Referer": "https://www.webtoons.com/"
						}
					}
				}
				return page;
			}
		});
	}
};