module.exports = 
{
	name:"mangafox",
	options:
	{
		type:"manga",
		latency:400,
		removeWatermarks:true
	},
	watermarkConfig:function(image)
	{
		var hashes = 
		[
			{
				area:{x:image.bitmap.width/2-344, y:image.bitmap.height-84, w:688, h:84},
				hash:["8U0x0200000", "a00w0100000"],													
				removeFunction:function(image){image.crop(0,0,image.bitmap.width,image.bitmap.height-90);}
			},
			{
				area:{x:image.bitmap.width/2-344, y:image.bitmap.height-84, w:688, h:84},
				hash:["ak0w0000000", "ak0w0100800", "aU0x0200000"],													
				removeFunction:function(image){image.crop(0,0,image.bitmap.width,image.bitmap.height-86);}
			},
			{
				area:{x:image.bitmap.width/2-297, y:image.bitmap.height-65, w:594, h:65},
				hash:["8E0w0200000", "ao000000000", "aU0w0200000", "a80w0200000", "8E000000000"],													
				removeFunction:function(image){image.crop(0,0,image.bitmap.width,image.bitmap.height-67);}
			},
			{
				area:{x:image.bitmap.width/2-297, y:image.bitmap.height-46, w:594, h:46},
				hash:["aw000000000"],													
				removeFunction:function(image){image.crop(0,0,image.bitmap.width,image.bitmap.height-48);}
			}
		];
		return hashes;
	},
	mextract:function(ng)
	{
		return ng.mmShared.commonMextract(
		{
			requestCfg:{url:function(i){return "https://fanfox.net/directory/"+(i+1)+".html?az";}},
			options:
			{
				gesiter:1500,
				getmaxpage:function(dom)
				{
					var elems = dom.getElementsByClassName("pager-list-left")[0].getElementsByTagName("a");
					return elems[elems.length-2].innerText;
				}
			},
			selectFct: function(dom){return dom.getElementsByClassName("manga-list-1-list")[0].getElementsByTagName("li");},
			elementFct: function(elem)
			{
				return {
					name: elem.getElementsByTagName("a")[1].title || elem.getElementsByTagName("a")[1].innerText,
					link: "https://fanfox.net"+/(href=\")([^"]+)/g.exec(elem.getElementsByTagName("a")[0].outerHTML)[2]
				};
			},
		});
	},
	cextract: function(m,ng)
	{	
		return ng.mmShared.commonCextract(
		{
			requestCfg:{url:m.link.replace("://fanfox.net/","://m.fanfox.net/")},
			selectFct: function(dom)
			{
				return Array.from(dom.querySelectorAll(".chlist a")).reverse();
			},
			elementFct: function(elem)
			{
				elem.href = "https:"+/(href=\")([^"]+)/g.exec(elem.outerHTML)[2];
				var volregex = /(v)(\d+)/g.exec(elem.href);
				var volnumb = volregex?volregex[2]:null;
				var chanumb = /(c)(\d+(\.?\d*)*)/g.exec(elem.href)[2];
							
				return {"volnumb":volnumb,"chanumb":chanumb,"link":elem.href};
			}
		});
	}
};
