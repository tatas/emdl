# Common API

Most plugins can be created by using the shared functionality of `mmshared.js` and `mm-workershared.js`. For global settings and a more extensive explanation about mextract/cextract/pextract/iextract check the [complete examples](https://gitlab.com/CopyNippyJoin/emdl/tree/master/js/mmodels) or the [specification](https://gitlab.com/CopyNippyJoin/emdl/tree/master/docs/Specifications.md).

## API for mmshared

```javascript
	mextract:function(ng)
	{
		return ng.mmShared.commonMextract(
		{
			/**
			 * @param {Object} requestCfg - the angular $http or node request options, the url can also be a function with the current iteration number as argument
			 */
			requestCfg:{url:function(i){return "http://example.org/exampleList?page="+(i+1);},
			/**
			 * the mmShared settings
			 * @param {Boolean} nodeRequest=false - use node request instead of angular
			 * @param {Number} gesiter - maximum amount of iterations (for requestCfg urls)
			 * @param {Number} latency=0 - latency between iteration calls, requests will be concurrent if the latency equals 0
			 * @param {Boolean} cancelOnEmpty - cancel all requests after one iteration returns an empty array
			 * @param {Function} getmaxpage - you get the dom of the first url and can return the new gesiter
			 */
			options:
			{
				nodeRequest:true,
				gesiter:1500,
				latency:0,
				cancelOnEmpty: true,
				getmaxpage:function(dom){return dom.getElementById("maxpage").innerText;}
			},
			/**
			 * @param {Object} dom - the dom of an request
			 * @returns {Array} iterArr - an array for elementFct
			 */
			selectFct: function(dom){return dom.getElementsByTagName("a");},
			/**
			 * @param {Object} elem - an element of the selectFct array
			 * @param {Object} dom - the dom of an request
			 * @param {number} i - the current iteration number
			 * @returns {Object} mlistPart - an object according to the specification
			 */
			elementFct: function(elem){return {"name":elem.innerText,"link":elem.href};},
			/**
			 * @param {Boolean} debug - enables additional debug logging
			 */
			debug:true
		});
	}

	cextract: async function(m,ng)
	{
		//pretty much the same as above, but you can only use the nodeRequest option (and it's intended to be used for chapters of course)
		return ng.mmShared.commonCextract(...);
	}
```

## API for mm-workershared

```javascript
	//follows the same pattern as above, but without the ability to use any option (it will always be a node request)
	if(ev.data.method == "pextract")
		mmworkershared.commonPextract(...);
	//only elementFct is available
	if(ev.data.method == "iextract")
		mmworkershared.commonIextract(...);
```