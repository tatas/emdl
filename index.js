const electron = require('electron');
const {app} = electron;
const {BrowserWindow} = electron;
const {ipcMain} = require('electron')
const nativeImage = require('electron').nativeImage;
const isDev = require('electron-is-dev');
const windowStateKeeper = require('electron-window-state');
  
var mainWindow = null;

app.commandLine.appendSwitch('autoplay-policy', 'no-user-gesture-required');

app.on('window-all-closed', function() {
	app.quit();
});

app.on('ready', function() {

	let mainWindowState = windowStateKeeper(
	{
		defaultWidth: 1100,
		defaultHeight: 1000
	});

	mainWindow = new BrowserWindow(
	{
		'name': "emdl",
		'x': mainWindowState.x,
		'y': mainWindowState.y,
		'width': 0,
		'height': 0,
		'autoHideMenuBar': true,
		'backgroundColor': "#000000"
	});
	mainWindow.setSize(mainWindowState.width,mainWindowState.height);
	mainWindowState.manage(mainWindow);
    mainWindow.loadURL('file://' + __dirname + "/index.html");

	if(isDev)
		mainWindow.webContents.openDevTools();

    mainWindow.on('closed', function() {		
        mainWindow = null;
    });

});

ipcMain.on('ondragstart', (event, filePath, iconData) =>
{
    event.sender.startDrag(
	{
		file: filePath,
		icon: nativeImage.createFromDataURL(iconData)
    })
});