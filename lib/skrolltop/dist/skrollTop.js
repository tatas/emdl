/*!
 * skrollTop 1.0.1
 * https://github.com/alvarotrigo/skrollTop.js
 * @license MIT
 *
 * Copyright (C) 2018 alvarotrigo.com - A project by Alvaro Trigo
 */
(function (root, factory) {
  if ( typeof define === 'function' && define.amd ) {
    define([], (function () {
      return factory(root, root.document);
    }));
  } else if ( typeof exports === 'object' ) {
    module.exports = factory(root, root.document);
  } else {
    root.skrollTop = factory(root, root.document);
  }
})(typeof global !== 'undefined' ? global : typeof window !== 'undefined' ? window : this, (function (window, document) {
    'use strict';

    var g_activeAnimation;
    var self = {};

    //easeInOutCubic animation included in the library
    Math.easeInOutCubic = function (t, b, c, d) {
        if ((t/=d/2) < 1) return c/2*t*t*t + b;return c/2*((t-=2)*t*t + 2) + b;
    };

    self.stop = function(){
        g_activeAnimation = false;
    };

    /**
    * Simulates the animated scrollTop of jQuery. Used when css3:false or scrollBar:true or autoScrolling:false
    * http://stackoverflow.com/a/16136789/1081396
    */
   self.scrollTo = function(params) {
       var element = typeof params.element !== 'undefined' ? params.element : window;
       var to = params.to;
       var duration = typeof params.duration !== 'undefined' ? params.duration : 700;
       var callback = typeof params.callback !== 'undefined' ? params.callback : null;
       var easing = typeof params.easing !== 'undefined' ? params.easing : Math.easeInOutCubic;
	   var scrollDirection = typeof params.scrollDirection !== 'undefined' ? params.scrollDirection : "scrollTop";
		
       var start;
	   if(scrollDirection == "scrollTop")
			start = element!==window ? element.scrollTop : (window.pageYOffset || document.documentElement.scrollTop)  - (document.documentElement.clientTop || 0);
	   if(scrollDirection == "scrollLeft")
		   start = element!==window ? element.scrollLeft : (window.pageXOffset || document.documentElement.scrollLeft)  - (document.documentElement.clientLeft || 0);
       var change = to - start;
       var currentTime = 0;
       var increment = 16.666; //same amount of milliseconds as requestAnimationFrame
       g_activeAnimation = true;

       var lastTime;
       
       var animateScroll = function(curTime) {
            if(!lastTime) 
                lastTime = curTime;
            increment = (curTime-lastTime);
            lastTime = curTime;
            //in case we want to stop it from other function whenever we want
           if (g_activeAnimation) {
               currentTime += increment;
               element[scrollDirection] = easing(currentTime, start, change, duration);
               if (currentTime < duration) {
                    requestAnimationFrame(animateScroll);
               } else if (callback){
                   callback();
               }
           }else if (currentTime < duration && callback){
               callback();
           }
       };

       requestAnimationFrame(animateScroll);
   };

   return self;
}));
