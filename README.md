# About
Emdl is an extensible manga (comic/webtoon/anime/gallery) downloader (reader/viewer) using Electron because slapping Chromium at something makes it better by default.
Aside from this it enables easy adding and editing of plugins without having to rebuild binaries from the source.
By using JavaScript the workflow from drafting in the web console to the creation of a working plugin is simplified.


You can get the binaries (win, mac, linux) [here](http://copynippyjoin.gitlab.io/emdl-website/).


The latest developer build (can be buggy/unstable) is available [here](https://gitlab.com/CopyNippyJoin/emdl/-/jobs/artifacts/master/download?job=deploy_build). 

Screenshots: [reader](http://copynippyjoin.gitlab.io/emdl-website/screenshots/screenshot1.jpg), 
[reader settings](http://copynippyjoin.gitlab.io/emdl-website/screenshots/screenshot2.jpg), 
[metadata](http://copynippyjoin.gitlab.io/emdl-website/screenshots/screenshot3.jpg), 
[downloader](http://copynippyjoin.gitlab.io/emdl-website/screenshots/screenshot4.jpg)

# Versioning
For releases [semantic versioning](http://semver.org/) is be used.

# Building
To build the application you need [node.js](https://nodejs.org/en/).
1. Clone the repository.
2. Change into its root directory and run `npm install` to install all dependencies.
3. Run `npm run start` to start the application or `npm run pack` to build the application.

# Plugin API
The plugins for the websites are located under `js/mmodels`, if you downloaded the binaries it may vary a bit (e.g. for win it should be `resources/app/src/js/mmodels`).
To add your own ones just paste them in this folder and it will work. 
Most websites are usually structured in a similar manner, thus new plugins can be implemented by simple copy & paste and adjusting some query selectors/regex (often less than 15 lines of code).

For every supported website there have to be two files.
<pre>
1. sitename.js        (functions will be executed in the main process        - will  block the UI)
2. sitename-worker.js (functions will be executed in a node.js child process - won't block the UI)
</pre>

For further documentation you can check out:
  - [The code of the existing plugins.](https://gitlab.com/CopyNippyJoin/emdl/tree/master/js/mmodels)
  - [The commons API](https://gitlab.com/CopyNippyJoin/emdl/tree/master/docs/CommonAPI.md), which simplifies the creation of plugins by offering shared functionality.
  - [The specification of the generic API.](https://gitlab.com/CopyNippyJoin/emdl/tree/master/docs/Specifications.md)

## Debugging
Pressing F12 will open the developer tools, to reload the page use F5.
There are quite a few out of box logging messages that will give you an idea where (and why) something may went wrong in your plugin.

Debugging of the sitename.js file will work in the usual way (`console.log()` or `debugger;`).

For sitename-worker.js `console.log()` (alias of `postMessage({"debug":debugObject});` if you did `require("mm-workershared.js");`) will have some limitations, because it's executed in a node child process.
If you need to log "real" objects you can use the web console to generate them.


# License
GPLv3
